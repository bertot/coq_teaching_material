
Require Import ZArith List Bool Lia predefined_functions.

Import ListNotations.

Open Scope Z_scope.

Module Failing_attempt_Zlist_min2.

Lemma Zlist_min2 a b :
  Zlist_min (a :: b :: nil) = Some (Z.min a b).
Proof.
unfold Zlist_min.
destruct (_ <=? _).
Abort.

End Failing_attempt_Zlist_min2.


Lemma Zlist_min_in2 a b :
  Zlist_min (a :: b :: nil) = Some (Z.min a b).
Proof.
unfold Zlist_min.
destruct (_ <=? _) eqn:cmp.
  assert (amin : a = Z.min a b).
    lia.
  rewrite <- amin.
  easy.
assert (bmin : b = Z.min a b).
  lia.
rewrite <- bmin.
easy.
Qed.