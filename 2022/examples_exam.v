Require Import List Arith Lia Bool.

(* Premier examen possible. *)
(****************************)
(* Dans quelles conditions la formule (x - y) vaut elle 0, lorsque
  x et y sont des entiers naturels. *)

(* On considère la fonction suivante, peut-on prouver que cette
 fonction effectue le même calcul que la fonction Nat.max.

  1/ en utilisant lia.
  2/ sans utiliser lia, mais en utilisant les théorèmes existants
     sur la soustraction et la fonction max, et les comparaisons
     entre entiers naturels. *)

Definition toy (n m : nat) := n - m + m.

Compute toy 3 5.

Compute toy 5 3.  

Compute toy 5 0.

(* Les trois lignes ci-dessus font partie de la question.  La
réponse commence ici. *)

Lemma toy_is_max (n m : nat) : toy n m = Nat.max n m.
Proof.
unfold toy.
lia.
Qed.

Lemma toy_is_max2 n m : toy n m = Nat.max n m.
Proof.
(* on fait un traitement par cas, suivant que n est plus petit
  que m ou non. *)
Search (_ <= _) (_ \/ _).
assert (cases: n <= m \/ m <= n).
  apply Nat.le_ge_cases.
destruct cases as [n_le_m | m_le_n].
  unfold toy.
(* On veut trouver les théorèmes qui parlent de soustraction et
  d'égalité à 0 *)
Search (_ - _) 0 (_ = _).
  assert (sub_is_0 : n - m = 0).
    rewrite Nat.sub_0_le.
    assumption.
  rewrite sub_is_0.
  rewrite Nat.add_0_l.
(* On veut trouver le nom du théorème qui dit quand le max est
  égal au deuxieme argument. *)
Search (Nat.max _ ?x = ?x).
  rewrite Nat.max_r.
    easy.
  easy.
unfold toy.
Search (_ - _ + _).
rewrite Nat.sub_add.
Search (Nat.max _ _) (_ <= _) (_ = _).
  rewrite Nat.max_l.
    easy.
  assumption.
assumption.
Qed.

(* La réponse à la question ci-dessus suffit pour cet examen. *)

(* Deuxieme examen possible. *)
(*****************************)
(* Ce deuxieme examen teste plus les connaissances vis-à-vis de
   la récursion et des preuves par récurrence. *)

(* Definir une fonction appelée count qui prend en argument:
  1/ un type A
  2/ une fonction f de type A -> bool
  3/ une liste dont les elements sont dans A
  et retourne le nombre d'elements de la liste pour lesquels
  la fonction retourne true.

On essaiera de faire ce qu'il faut pour que le premier argument
de la fonction soit implicite (si vous ne comprenez pas cette
remarque, cette partie de la question peut être ignorée). *)

(* Montrer que lorsque la fonction f est (fun x => true), le
  résultat est la longueur de la liste. *)

Fixpoint count {A : Type}(f : A -> bool)(l : list A) :=
  match l with
  | nil => 0
  | e :: tl => if f e then S (count f tl) else count f tl
  end.

Module sol2.

(* Réponse en ignorant la remarque sur l'argument implicite. *)
Fixpoint count (A : Type)(f : A -> bool)(l : list A) :=
  match l with
  | nil => 0
  | e :: tl => if f e then S (count A f tl) else count A f tl
  end.

End sol2.

Lemma count_true_length :
  forall A (l : list A), count (fun x => true) l = length l.
Proof.
intros A l.
induction l as [ | e tl Ih].
  easy.
simpl.
rewrite Ih.
easy.
Qed.

Module sol2_length.

Import sol2.

Lemma count_true_length :
  forall A (l : list A), count A (fun x => true) l = length l.
Proof.
intros A l.
induction l as [ | e tl Ih].
  easy.
simpl.
rewrite Ih.
easy.
Qed.

End sol2_length.
