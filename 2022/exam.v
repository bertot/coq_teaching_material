Require Import Arith List Bool Lia.

(* Write a function of type nat -> bool that detects if a number is a 
   multiple of 3.  You can use an existing division function, if one
   already exists. *)

(* Show that this function is correct with respect to a definition of
  multiples based on Nat.divide *)

(* express and prove a lemma saying that 0 is not a power of 3 *)

(* Prove the following lemma. *)
Lemma power_diag_ge (n : nat) : n <= n ^ n.


(* Write a function of type nat -> bool that detects if a number is a power
  3 *)

(* What lemma would express that this function is correct? *)

(* Can you prove the correctness. *)
