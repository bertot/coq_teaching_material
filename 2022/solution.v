Require Import Arith List Bool Lia.

(* Write a function of type nat -> bool that detects if a number is a 
   multiple of 3.  You can use an existing division function, if one
   already exists. *)

Search (_ mod _ = 0).

Definition multiple_of_3 (n : nat) : bool := n mod 3 =? 0.

(* Show that this function is correct with respect to a definition of
  multiples based on Nat.divide *)

Lemma multiple_of_3_correct n : multiple_of_3 n = true <-> Nat.divide 3 n.
Proof.
rewrite <- Nat.mod_divide.
unfold multiple_of_3.
rewrite Nat.eqb_eq.
easy.
easy.
Qed.

(* express and prove a lemma saying that 0 is not a power of 3 *)
Lemma power3_not_0 : ~ exists k, 0 = 3 ^ k.
Proof.
intros [k kP].
assert (abs : 3 ^ 0 <= 3 ^ k).
  apply Nat.pow_le_mono_r; lia.
rewrite <- kP in abs; simpl in abs.
rewrite Nat.le_0_r in abs; discriminate.
Qed.

(* Prove the following lemma. *)
Lemma power_diag_ge (n : nat) : n <= n ^ n.
Proof.
destruct n as [ | n].
  simpl; lia.
replace (S n) with ((S n) ^ 1) at 1.
  apply Nat.pow_le_mono_r; try easy.
  lia.
rewrite Nat.pow_1_r; easy.
Qed.

Lemma power_diag_ge' (n : nat) : n <= n ^ n.
induction n as [ | [ | p] Ih].
    lia.
  simpl; lia.
apply le_trans with (S (S p ^ S p)).
  lia.
change (S p ^ S p < S (S p) ^ S (S p)).
apply le_lt_trans with (S p ^ S (S p)).
  apply Nat.pow_le_mono_r.
    discriminate.
  lia.
apply Nat.pow_lt_mono_l; lia.
Qed.

(* Write a function of type nat -> bool that detects if a number is a power
  3 *)

Fixpoint power_of_3_aux (n : nat)(fuel : nat) : bool :=
  match fuel with
  | S p =>
     match n with
     | 0 => false
     | 1 => true
     | x => if n mod 3 =? 0 then power_of_3_aux (n / 3) p else false
     end
  | 0 => if n =? 0 then false else true
  end.

Definition power_of_3 (n : nat) : bool := power_of_3_aux n n.

Fixpoint power_of_3_aux' (n : nat) (fuel : nat) :=
  match fuel with
  | 0 => if n =? 1 then true else false
  | S p =>
    if n =? (3 ^ fuel) then true else power_of_3_aux' n p
  end.

(* we use the logarithm in base 2 to avoid starting with a value
  that is too large. *)
Definition power_of_3' (n : nat) := power_of_3_aux' n (S (Nat.log2 n)).

(* The third solution is more efficient as it does not use the fuel
  and starts computing powers of 3 from the bottom. *)
Fixpoint power_of_3_aux'' (n rank fuel : nat) : bool :=
match fuel with
| 0 => false
| S p =>
  let v := 3 ^ rank in
    if n <? v then false
    else if n =? v then true
    else power_of_3_aux'' n (S rank) p
end.

Definition power_of_3'' (n : nat) :=
  power_of_3_aux'' n 0 (S n).

(* What lemma would express that this function is correct? *)
Definition power_of_3_spec (f : nat -> bool) :=
   forall x, f x = true <-> exists k, x = 3 ^ k.


Lemma power_of_3_correct (n : nat) : power_of_3_spec power_of_3.
Proof.
unfold power_of_3_spec.
unfold power_of_3.
assert (not_0_pow_3 : ~ exists k, 0 = 3 ^ k).
intros [[ |k] pK].
  easy.
  assert (step : 3 ^ 1 <= 3 ^ S k).
    apply Nat.pow_le_mono_r; lia.
  enough (3 <= 3 ^ S k) by lia.
  apply Nat.le_trans with (2 := step).
  simpl; lia.
assert (forall y x, x <= 3 ^ y -> power_of_3_aux x y = true <->
             (exists k, x = 3 ^ k)).
  induction y as [ | p Ih].
    simpl.
    intros [ | x]; simpl.
      intros _; simpl; split; easy.
    intros xle0'; assert (xle0 : x = 0) by lia.
    rewrite xle0.
    split; try easy.
    intros _; exists 0; easy.
  intros x xbound; cbv [power_of_3_aux].
  destruct x as [ | x].
    easy.
    destruct x as [ | x].
    split;[ | easy ].
    intros _; exists 0; easy.
    destruct (S (S x) mod 3 =? 0) eqn:cmp.
    rewrite Nat.eqb_eq in cmp.
    rewrite Ih.
      split.
        intros [k pK]; exists (S k).
        rewrite (Nat.div_mod_eq (S (S x)) 3).
        rewrite cmp, Nat.add_0_r.
        rewrite pK; easy.
      intros [[ | k] pK];[discriminate | ].
      exists k.
      rewrite (Nat.div_mod_eq (S (S x)) 3), cmp, Nat.add_0_r in pK.
      change (3 ^ S k) with (3 * 3 ^ k) in pK.
      now rewrite Nat.mul_cancel_l in pK.
    rewrite (Nat.div_mod_eq (S (S x)) 3) in xbound.
    rewrite cmp, Nat.add_0_r in xbound.
    change (3 ^ S p) with (3 * 3 ^ p) in xbound.
    now apply Mult.mult_S_le_reg_l_stt in xbound.
  split;[easy | ].
  intros [ [ | k] pK];[easy | ].
  enough (S (S x) mod 3 = 0) by (rewrite Nat.eqb_neq in cmp; lia).
  rewrite pK; replace (3 ^ S k) with (3 ^ k * 3) by (simpl; ring).
  now rewrite Nat.mod_mul.
intros x; rewrite H; try easy.
induction x.
  simpl; lia.
replace (3 ^ S x) with (2 * 3 ^ x + 3 ^ x) by (simpl; ring).
replace (S x) with (1 + x) by ring.
apply Nat.add_le_mono;[ | easy].
enough (1 <= 3 ^ x) by lia.
replace 1 with (3 ^ 0) at 1 by (simpl; ring).
apply Nat.pow_le_mono_r; lia.
Qed.

Lemma pow_3_lt_log2 x : x < 3 ^ S (Nat.log2 x).
Proof.
destruct x as [ | [ | x]].
    simpl; lia.
  simpl; lia.
assert (cnd : 0 < S (S x)) by lia.
destruct (Nat.log2_spec _ cnd) as [_ it].
apply Nat.lt_le_trans with (1 := it).
apply Nat.pow_le_mono_l.
lia.
Qed.

Lemma pow_3_le_log2 x : x <= 3 ^ Nat.log2 x.
Proof.
destruct x as [ | [ | [ | [ | x]]]]; try (simpl; lia).
set (w := S (S (S (S x)))).
assert (tmp : 2 <= Nat.log2 w).
  change 2 with (Nat.log2 4).
  apply Nat.log2_le_mono; unfold w; lia.
assert (big : 2 ^ Nat.log2 w <= w < 2 ^ S (Nat.log2 w)).
  apply Nat.log2_spec; lia.
destruct big as [_ small].
enough (2 ^ S (Nat.log2 w) <= 3 ^ Nat.log2 w) by lia.
revert tmp; clear small; induction 1 as [ | p pge2 Ih].
  simpl; lia.
replace (2 ^ S (S p)) with (2 ^ S p + 2 ^ S p) by (simpl; ring).
replace (3 ^ S p) with (2 * 3 ^ p + 3 ^ p) by (simpl; ring).
apply Nat.add_le_mono.
  lia.
lia.
Qed.

Lemma power_of_3'_correct : power_of_3_spec power_of_3'.
Proof.
unfold power_of_3_spec, power_of_3'.
assert (main : forall y x, 1 <= y -> power_of_3_aux' x y = true <->
                (exists k, x = 3 ^ k /\ k <= y)).
  intros y x yge1; revert x.
  induction yge1 as [ | y yge1 Ih]; intros x.
    destruct x as [ | x]; simpl.
      split;[discriminate | ].
      now intros [k [p3k0 _]]; case power3_not_0; exists k.
    destruct (x =? 2) eqn:cmp.
      rewrite Nat.eqb_eq in cmp; split; [intros _ | easy].
      exists 1; simpl; lia.
    rewrite Nat.eqb_neq in cmp.
    destruct (x =? 0) eqn:cmp0.
      split;[intros _ | easy].
      rewrite Nat.eqb_eq in cmp0; exists 0; simpl; lia.
    split;[easy | ].
    rewrite Nat.eqb_neq in cmp0.
    intros [[ | k] kP]; simpl in kP.
      lia.
    assert (k0 : k = 0) by lia.
    rewrite k0 in kP; simpl in kP; lia.
  cbn [power_of_3_aux'].
  destruct (x =? 3 ^ S y) eqn:cmp.
    rewrite Nat.eqb_eq in cmp; split;[ intros _ | easy].
    exists (S y); split;[tauto | lia].
  rewrite Nat.eqb_neq in cmp.
  rewrite Ih; split; intros [k kP]; exists k; (split;[tauto | ]).
    lia.
  destruct kP as [xp3k klsy].
  apply Lt.le_lt_or_eq_stt in klsy; destruct klsy as [below | here].
    lia.
  rewrite here in xp3k; contradiction.
intros x; split.
  rewrite main;[ | lia].
  intros [k [kP _]]; exists k; exact kP.
intros [k kP].
assert (step : exists k, x = 3 ^ k /\ k <= S (Nat.log2 x)).
  exists k; split;[ easy | ].
  destruct (le_lt_dec k (S (Nat.log2 x))) as [it | abs]; auto.
  assert (tmp := pow_3_lt_log2 x).
  apply (Nat.pow_lt_mono_r 3) in abs; lia.
rewrite <- main in step; auto.
lia.
Qed.

Lemma power_of_3''_correct : power_of_3_spec power_of_3''.
Proof.
unfold power_of_3_spec, power_of_3''.
assert (main : forall y x r, x < 3 ^ (r + y) -> 
          (forall s, s < r -> x <> 3 ^ s) ->
          power_of_3_aux'' x r y = true <-> (exists k, x = 3 ^ k)).
  induction y as [ | y Ih].
    intros x r.
    rewrite Nat.add_0_r.
    intros xbound no_witness.
    simpl.
    split;[easy | ].
    intros [k abs].
    assert (k < r).
      rewrite (Nat.pow_lt_mono_r_iff 3);[ | lia].
      now rewrite <- abs.
    now case (no_witness k).
  intros x r xbound no_witness.
  simpl.
  destruct (x <? 3 ^ r) eqn:cmp.
    rewrite Nat.ltb_lt in cmp.
    split;[easy | ].
    intros [k abs].
    assert (k < r).
      rewrite (Nat.pow_lt_mono_r_iff 3);[ | lia].
      now rewrite <- abs.
    now case (no_witness k).
  destruct (x =? 3 ^ r) eqn:cmp2.
    rewrite Nat.eqb_eq in cmp2.
    split;[intros _ | easy].
    now exists r.
  rewrite Nat.ltb_ge in cmp.
  rewrite Nat.eqb_neq in cmp2.
  assert (xbound' : x < 3 ^ (S r + y)).
    now replace (S r + y) with (r + S y) by ring.
  apply Ih; auto.
  intros s; rewrite Nat.lt_succ_r.
  rewrite Nat.le_lteq; intros [sltr | seqr].
    now apply no_witness.
  rewrite seqr; exact cmp2.
intros x; apply main.
  rewrite Nat.add_0_l.
  induction x as [ | x Ih];[simpl; lia | rewrite Nat.pow_succ_r; lia].
intros s abs; lia.
Qed.
