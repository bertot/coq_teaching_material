Require Import Arith Lia List Bool.

Fixpoint F (n a b : nat) : bool :=
  match n with
  | 0 => true
  | S n' =>
    (if n' <? a then
       false
     else
       F (n' - a) a b) ||
    (if n' <? b then
       false
     else
       F (n' - b) a b)
  end.

Lemma F_correct n a b :
  F n a b = true <-> exists x y, x * S a + y * S b = n.
Proof.
induction n using (well_founded_induction lt_wf).
destruct n as [ | n'].
  split.
    intros _; exists 0, 0; easy.
  easy.
cbn [F].
destruct (n' <? a) eqn:cmpa.
  destruct (n'<? b) eqn:cmpb.
    split.
      discriminate.
    intros [x [y pxy]].
    destruct x as [ | x'].
      destruct y as [ | y'].
        discriminate pxy.
      simpl in pxy.
Search (_ <? _ = true).      
      rewrite Nat.ltb_lt in cmpb.
      lia.
    simpl in pxy.
    rewrite Nat.ltb_lt in cmpa.
    lia.
  cbn [orb].
  rewrite H.
    split.
      intros [x [y pxy]].
      exists x, (S y).
      replace n' with ((n' - b) + b).
        rewrite <-pxy.
        lia.
Search (_ <=? _ = false).
      rewrite Nat.ltb_ge in cmpb.
      lia.
    intros [x [y pxy]].
    rewrite Nat.ltb_lt in cmpa; rewrite Nat.ltb_ge in cmpb.
    destruct y as [ | y'].
      simpl in pxy.
      destruct x as [ | x'].
        lia.
      simpl in pxy; lia.
    exists x, y'.
    lia.
  lia.
rewrite Nat.ltb_ge in cmpa.
destruct (n' <? b) eqn: cmpb.
  rewrite Nat.ltb_lt in cmpb.
  rewrite orb_false_r.
  rewrite H.
    split.
      intros [x [y pxy]].
      exists (S x), y.
      simpl.
      lia.
    intros [x [y pxy]].
    destruct y as [ | y'].
      destruct x as [ | x'].
        discriminate pxy.
      exists x', 0.
      simpl in pxy.
      lia.
    simpl in pxy.
    lia.
  lia.
rewrite Nat.ltb_ge in cmpb.
split.
Search (_ || _ = true).
  rewrite orb_true_iff.
  intros orH; destruct orH as [by_a | by_b].
    rewrite H in by_a.  
      destruct by_a as [x [y pxy]].
      exists (S x), y.
      simpl.
      lia.
    lia.
  rewrite H in by_b.
    destruct by_b as [x [y pxy]].
    exists x, (S y).
    simpl.
    lia.
  lia.
intros [x [y pxy]].
destruct x as [ | x'].
  destruct y as [ | y'].
    discriminate pxy.
  rewrite orb_true_iff.
  right.
  rewrite H.
    exists 0, y'.
    simpl in pxy.
    lia.
  lia.
simpl in pxy.
rewrite orb_true_iff.
left.
rewrite H.
  exists x', y.
  lia.
lia.
Qed.

(* With the existing function filter, we can compute the list of all values
  that satisfy the boolean function F .. 3 8 *)
Compute filter (fun n => F n 3 8) (seq 0 24).

Compute seq 0 24.  (* 24 elements, ending in 23 *)

Lemma filter_F_correct a b m k:
  k < m ->
  In k (filter (fun n => F n a b) (seq 0 m)) <-> 
  exists x y, x * S a + y * S b = k.
Proof.
Search filter.
intros kltm.
rewrite filter_In.
Search seq.
rewrite in_seq.
rewrite Nat.add_0_l.
rewrite F_correct.
split.
  intros [dummy k_condition]; exact k_condition.
intros k_condition.
split.
  (* Here lia would also work. *)
  split.
    apply Nat.le_0_l.
  assumption.
assumption.
Qed.

Lemma above_24 n :
  24 <= n -> exists x y, x * 4 + y * 9 = n.
Proof.
intros H; induction H as [ | m mge24 Ihm].
  exists 6, 0.
  easy.
destruct Ihm as [x [ y pxy]].
Search (_ <= _ \/ _ < _).
assert (cases : y <= 2 \/ 2 < y).
  lia.
destruct cases as [case2 | case1].
  assert (xge2 : 2 <= x) by lia.
  exists (x - 2), (y + 1).
  lia.
exists (x + 7), (y - 3).
lia.
Qed.

Lemma main_statement :
  forall n, 
    (In n (0 :: 4 :: 8 :: 9 :: 12 :: 13 :: 16 ::
             17 :: 18 :: 20 :: 21 :: 22 :: nil) \/
      24 <= n) <->
    exists x y, x * 4 + y * 9 = n.
Proof.
intros n.
assert (cases : 24 <= n \/ n < 24).
  lia.
destruct cases as [large | small].
  split.
    intros _; apply above_24.
    easy.
  right.
  easy.
split.
  intros [inlist | nge24].
    rewrite <- (filter_F_correct 3 8 24).
    (* Here Coq performs the computation of the list and checks that it
       has the same content as what we said. *)
      exact inlist.
    exact small.
  lia.
rewrite (filter_F_correct 3 8 24).
  intros result.
  left.
  exact result.
assumption.
Qed.
