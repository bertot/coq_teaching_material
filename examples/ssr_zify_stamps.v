From mathcomp Require Import all_ssreflect zify.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Definition accessible a b n :=
   exists x y, x * a + y * b = n.

Lemma all_ge_24_accessible n :
    24 <= n -> accessible 4 9 n.
Proof.
elim/ltn_ind: n => n Ih.
have [/eqP /[dup] at24 -> | above] := boolP(n == 24).
  by move=> _; exists 6, 0.
move=> gt23.
have nm1n : n.-1 < n by lia.
have nm1gt23: 23 < n.-1 by lia.
have [x [y pxy]] := Ih _ nm1n nm1gt23.
rewrite -(ltn_predK gt23) -pxy.
have [yge3 | ] := boolP(3 <= y); last rewrite -leqNgt => yle2.
  exists (x + 7), (y - 3); lia.
have xgt1 : 1 < x by lia.
exists (x - 2), (y + 1); lia.
Qed.

Definition accessible_pred n a b :=
  has (fun k => (a * k <= n) && (b %| n - a * k)) (iota 0 (n %/ a).+1).

Lemma accessibleP n a b : 
  a != 0 -> b != 0 ->
  reflect (exists x y, x * a + y * b = n) (accessible_pred n a b).
Proof.
intros an0 bn0; have [ac | nac] := boolP(accessible_pred n a b).
  apply: ReflectT.
  move/hasP: ac=> [k kp1 /andP [kp2 kp3]]; exists k, ((n - a * k) %/ b). 
  rewrite -[_ * b]addn0 -(eqP kp3) -divn_eq mulnC; lia.
apply: ReflectF.
have agt0 : 0 < a by lia.
move=> [x [y pxy]]; move: nac => /hasPn /(_ x).
have xin : x \in iota 0 (n %/ a).+1.
  rewrite mem_iota add0n /= -(@ltn_pmul2r a) //.
  suff : n < (n %/ a).+1 * a by lia.
  rewrite mulSn addnC [X in X < _](divn_eq n a); lia.
move=> /(_ xin) /negP; apply.
apply/andP; split; first by lia.
by rewrite -pxy mulnC addKn dvdn_mull.
Qed.

Lemma filter_accP m n a b k :
  a != 0 -> b != 0 ->
  reflect (m <= k < m + n /\ exists x y, x * a + y * b = k)
    (k \in [seq i <- iota m n | accessible_pred i a b]).
Proof.
move=> an0 bn0.
have [ | ] := boolP(k \in [seq i <- iota m n | accessible_pred i a b]).
  rewrite mem_filter => /andP[] /accessibleP => /(_ an0 bn0) exstmt.
  rewrite mem_iota => intstmt.
  by apply: ReflectT.
move/negP=> revert.
apply ReflectF=> -[intstmt exstmt].
by apply: revert; rewrite mem_filter mem_iota intstmt andbT; apply/accessibleP.
Qed.

Lemma main :
  forall n, ((n \in [:: 0; 4; 8; 9; 12; 13; 16; 17; 18; 20; 21; 22]) ||
             (23 < n)) -> exists x y, x * 4 + y * 9 = n.
Proof.
move=> n ncond.
have [small | large] := boolP(n < 24).
  have small' : 23 < n = false by lia.
  move: ncond; rewrite small' orbF.
  by move => /(filter_accP 0 24 n (isT : 4 != 0) (isT : 9 != 0)) []. 
by apply: all_ge_24_accessible; lia.
Qed.
