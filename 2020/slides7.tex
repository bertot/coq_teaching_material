\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{alltt}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{Dependent types}
\author{Yves Bertot}
\date{November 2020}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Objectives}
\begin{itemize}
\item Functions with dependent types
\item Constructing proofs by function composition
\item Dependently typed pattern-matching
\item Dependent inductive types
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Typing rules for functions: easy case}
\begin{itemize}
\item If function \(f\) has type \(A \rightarrow B\)
\item If value \(x\) has type \(A\)
\item Then \(f~x\) is well-formed and has type \(B\)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Typing rules for functions: advanced case}
\begin{itemize}
\item If function \(f\) has type \(\forall x : A, B\)
\item If value \(e\) has type \(A\)
\item Then \(f~e\) is well-formed
  \item The result has type \(B [x\leftarrow e]\)
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Simple typing illustration}
\begin{alltt}
Require Import ZArith.

Open Scope Z_scope.

Check Z.opp.
\textcolor{blue}{Z.opp : Z -> Z}
Check 2.
\textcolor{blue}{2 : Z}
Check Z.opp 2.
\textcolor{blue}{- (2) : Z}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Advanced typing illustration}
\begin{alltt}
Require Import ZArith.

Open Scope Z_scope.

Section sandbox.
Variable F : Z -> Type.

Variable g : forall x : Z, F x.

Check g.
\textcolor{blue}{g : forall x : Z, F x}
Check 2.
\textcolor{blue}{2 : Z}
Check g 2.
\textcolor{blue}{g 2 : F 2}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Illustration with proofs}
\begin{alltt}
Check le_n.
\textcolor{blue}{le_n : forall x : nat, (x <= x)%nat}

Check le_S.
\textcolor{blue}{e_S : forall n m : nat, (n <= m)%nat -> (n <= S m)%nat}

Check le_n 3.
\textcolor{blue}{le_n 3 : (3 <= 3)%nat}

Check le_S 3 3 (le_n 3).
\textcolor{blue}{le_S 3 3 (le_n 3) : (3 <= 4)%nat}
\end{alltt}
\begin{itemize}
\item {\tt le\_S 3 3 (le\_n 3)} is a proof of {\tt (3 <= 4)\%nat}
\item How do you construct a proof of {\tt (3 <= 5)\%nat}?
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Illustration with bounded integers}
\begin{alltt}
Require Import ZArith Lia.

Open Scope Z_scope.

Record upper_bounded (k : Z) :=
   mkub \{ toZ : Z; ubP : toZ < k\}.

Check mkub.
\textcolor{blue}{mkub : forall k toZ : Z, toZ < k -> upper_bounded k}
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Explanations for {\tt upper\_bound} and {\tt mkub}}
\begin{itemize}
\item {\tt upper\_bounded} describes integers that are guaranteed to be smaller than a given bound
\item {\tt mkub} is the function that produces elements in this datatype
\item The first argument of {\tt mkub} is an integer (the upper bound)
\item The secong argument of {\tt mkub} is an integer (named {\tt toZ} here)
\item The third argument is a proof
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Making a default element of type {\tt upper\_bounded}}
\begin{alltt}
Lemma Zm1lt x : x - 1 < x. Proof. lia. Qed.

Definition maxub (k : Z) : upper_bounded k :=
  mkub k (k - 1) (Zm1lt k).

Check maxub.
\textcolor{blue}{maxub : forall k, upper_bounded k.}

Check maxub 3.
\textcolor{blue}{maxub 3 : upper_bounded 3}

Arguments toZ [k].

Compute toZ (maxub 3).
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Converting a bounded number into a plain number}
\begin{itemize}
\item Function {\tt toZ} takes two arguments and returns an integer
\item The first argument is the bound {\tt k}
\item The second argument is an element of type {\tt upper\_bounded k}
\item The result is the integer stored in the {\tt upper\_bounded} record
\item The first argument is made implicit: we shall not need to write it
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Converting an arbitrary integer into a bounded one}
\begin{itemize}
\item {\tt maxub} does not call any testing function
\begin{itemize}
\item But the value is fixed by the type
\end{itemize}
\item When programming we can check that a property is satisfied
\item Illustration requires more advanced programming concepts
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Knowledge from testing}
\begin{alltt}
Definition example (b : bool) :=
  match n with
  | true => (* Here we should have a fact
            saying b = true *)
  | false => (* Here we should have a fact
              saying b = false *)
  end.
\end{alltt}
\begin{itemize}
\item The dreams expressed in comments above are not satisfied in conventional programming
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Knowledge from testing : do it}
\begin{alltt}
Definition bool_dep (P : bool -> Type)
  (ft : P true) (ff : P false)
  (b : bool) : P b :=
  match b with | true => ft | false => ff end.

Check bool_dep.
\textcolor{blue}{bool_dep : forall P : bool -> Type,
    P true -> P false -> forall b : bool, P b}
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Novelty of the previous slide}
\begin{itemize}
\item The values {\tt ft} and {\tt ff} do not have the same type!
\item Actually three instances of {\tt P} appear in the pattern matching expression
\begin{itemize}
\item {\tt P b} the type of the whole pattern-matching expression
\item {\tt P true} the type of the expression in the first branch
\item {\tt P false} the type of the expression in the second branch
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Good choice of {\tt P}}
\begin{itemize}
\item assume {\tt x} {\tt k} are fixed
\item Consider {\tt P b : x <? k = b -> upper\_bounded k}
\item This a dependent family of types on boolean
\item Type {\tt P true} is for a function that can use a proof of the fact
   {\tt x <? y = true}
\item Type {\tt P false} is for a function that can use a proof of the fact
   {\tt x <? y = false}
\item Type {\tt P (x <? k)} is {\tt x <? k = x <? k -> upper\_bounded k}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Learning from a boolean comparison}
\begin{alltt}
Definition Zltb_to_lt x y : x <? y = true -> x < y :=
   proj1 (Z.ltb_lt x y).

Definition below_bound_case (k x : Z) :
   x <? k = \textcolor{red}{true} -> upper_bounded k :=
  fun h : x <? k = true =>
     mkub k x (Zltb_to_lt x k h).

Definition above_bound_case (k x : Z) :
   x <? k = \textcolor{red}{false} -> upper_bounded k :=
   fun h => maxub k.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Learning from a boolean comparison (2)}
\begin{alltt}
Check fun k x =>
  bool_dep (fun b => x <? k = b -> upper_bounded k)
    (below_bound_case k x)
    (above_bound_case k x).
\textcolor{blue}{forall (k x : Z) (b : bool),
    (x <? k) = b -> upper_bounded k}

Check fun k x =>
  bool_dep (fun b => x <? k = b -> upper_bounded k)
    (below_bound_case k x) (above_bound_case k x)
    \textcolor{red}{(x <? k)}.
\textcolor{blue}{forall k x : Z, (x <? k) = (x <? k) -> upper_bounded k}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Learning from a boolean comparison (3)}
\begin{alltt}
Check fun k x =>
  bool_dep (fun b => x <? k = b -> upper_bounded k)
    (below_bound_case k x) (above_bound_case k x)
    \textcolor{red}{(x <? k) refl_equal}.
\textcolor{blue}{forall k : Z, Z -> upper_bounded k}

Definition test_mkub k x : upper_bounded k :=
  bool_dep (fun b => x <? k = b -> upper_bounded k)
    (below_bound_case k x) (above_bound_case k x)
    (x <? k) refl_equal.

Check test_mkub 10 3.

Compute toZ (test_mkub 10 3).
\textcolor{blue}{3}
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Explanation}
\begin{itemize}
\item Thanks to {\tt bool\_dep} and the use of equalities, we
can have an extra hypothesis in each branch
\item That extra hypothesis tells what test was performed
\item This can be used to construct the proof required in {\tt mkub}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Already provided in Coq}
What I called {\tt bool\_dep} exists in Coq with name {\tt bool\_rect}
\end{frame}
\begin{frame}
\frametitle{Functions as proofs}
\begin{itemize}
\item Function code can also be constructed by tactics
\item Applying a function is expressed by the tactic {\tt apply}
\item Writing a {\tt fun ... => ...} is expressed by {\tt intros}
\item Writing a {\tt match} statement is done by {\tt destruct}
\item Writing a {\tt match} with extra equality hypotheses is done by {\tt destruct ... eqn:...}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example building {\tt test\_mkub} as a proof}
\begin{alltt}
Definition test_mkub2 (k n : Z) : upper_bounded k.
destruct (n <? k) eqn:h.
  apply (mkub k n).
  apply Z.ltb_lt.
  exact h.
apply maxub.
Defined.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{The witness pattern}
\begin{itemize}
\item Coq provides an inductive type noted {\tt \{x : A | P x\}}
\begin{itemize}
\item {\em values of type {\tt A} that satisfy {\tt P}}
\item produced by constructor {\tt exist}\\
{\tt exist~:~forall A (P : A -> Prop) (x : A),\\
~~~~~~~~ \{x : A | P x\}}
\item usable in pattern-matching
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Generic witness function}
\begin{alltt}
Definition witness \{A : Type\}(a : A) :
   \{b : A | a = b\} :=
  exist (fun b' => a = b') a eq_refl.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Witness use case}
\begin{alltt}
Definition test_mkub3 (k x : Z) : upper_bounded k :=
  match witness (x <? k) with
  | exist _ true h =>
    (* here h is a proof that x <? k = true *)

    mkub k x (proj1 (Z.ltb_lt x k) \textcolor{red}{h})

  | exist _ false _ => maxub k
  end.
\end{alltt}
\begin{itemize}
\item A match on {\tt witness \(e\)} is like a match on \(e\)
\item Produces an extra hypothesis expressing what computation was performed
\item More elegant than relying on {\tt bool\_dep}
\item Both branches of the pattern-match have the same return type
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Dependent pattern-matching on natural numbers}
\begin{alltt}
Definition nat_dep_case (P : nat -> Prop) (h0 : P O)
  (hs : forall k, P (S k)) (n : nat) : P n :=
  match n with
  | O => h0
  | S p => hs p
  end.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Dependent pattern-matching with recursion}
\begin{alltt}
Fixpoint nat_dep_rec (P : nat -> Prop) (h0 : P O)
  (hs : forall k, P k -> P (S k)) (n : nat) : P n :=
  match n with
  | O => h0
  | S p => hs p (nat_dep_rec P h0 hs p)
  end.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Dependent types in inductive types}
\begin{itemize}
\item We already saw some: {\tt upper\_bounded}
\item We can also add recursion
\item Dependent inductive types are the foundation of Coq
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{recursive inductive dependent type}
\begin{alltt}
Inductive t2 : nat -> Type :=
| start2 : t2 O
| step2 : forall n, t2 n -> t2 (S (S n)).
\end{alltt}
\begin{itemize}
\item Type {\tt t2 \(n\)} has an element exactly when \(n\) is even
\item Many interesting concepts can be defined this way
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Dependent singleton type}
\begin{alltt}
Inductive singleton3 : Z -> Prop :=
  is_3 : singleton3 3.

Definition singleton3_dep (x : Z) (prf : singleton3 x) 
  (P : Z -> Prop) (p3 : P 3) : P x :=
  match prf with is_3 => p3 end.
\end{alltt}
\begin{itemize}
\item {\tt singleton3} is satisfied only once.
\item In pattern matching rule, we used the fact that the constructor is
explicitely used for 3
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Parameterized singleton type}
\begin{alltt}
Inductive singleton \{A : Type\} (x : A) : A -> Prop :=
  single : singleton x x.

Definition singleton_dep
   \{A : Type\} \{x y : A\} (prf : singleton x y)
   (P : A -> Prop) (px : P x)  : P y :=
  match prf in singleton _ b return P b with
  | single _ => px
  end.  
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Singleton type as equality}
\begin{alltt}
Check single.
\textcolor{blue}{single : forall x : ?A, singleton x x}

check singleton_dep.
\textcolor{blue}{singleton_dep
     : singleton ?x ?y -> forall P : ?A ->
       Prop, P ?x -> P ?y}
\end{alltt}
\begin{itemize}
\item {\tt singleton x y} really means {\tt x = y}
\item {\tt apply singleton\_dep H } really means {\tt rewrite <- H}
\item This teaches us that inductive types are at the foundation Coq
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
