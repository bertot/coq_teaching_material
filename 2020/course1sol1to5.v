Require Import ZArith List.
Import ListNotations.

Fixpoint mullist (l : list Z) : Z :=
  match l with
  | nil => 1 (* This was not specified, *)
    (* but it makes the code easier to choose 1. *)
  | a :: tl =>  a * mullist tl
  end.

Fixpoint has0 (l : list Z) : bool :=
  match l with
  | nil => false
  | a :: tl => if (a =? 0)%Z then true else has0 tl
  end.

Fixpoint eq_nat (n m : nat) : bool :=
  match n with
  | O => match m with 0 => true | _ => false end
  | S n' => match m with 0 => false | S m' => eq_nat n' m' end
  end.

Fixpoint mklist (n a : nat) : list nat :=
  match n with
  | 0 => nil
  | S p => a :: mklist p a
  end.

Fixpoint nsucc (n a : nat) : list nat :=
  match n with 0 => nil | S p => a :: nsucc p (a + 1) end.

Compute nsucc 3 2.
Compute mklist 3 2.
Compute eq_nat 3 2.
Compute eq_nat 3 3.
Compute has0 [3;2;1;0;1;2;3]%Z.
Compute mullist [316;316]%Z.
