Require Import ZArith Arith Lia List.

Fixpoint factorial (n : nat) : nat :=
  match n with O => 1 | S p => S p * factorial p end.

Compute factorial 3.

Definition pfact (n : positive) : positive :=
  snd (Pos.iter (fun '(x, y) => (x + 1, x * y))%positive (1, 1)%positive n).

Lemma Pos_succ_correct (p : positive) :
   Pos.to_nat (Pos.succ p) = S (Pos.to_nat p).
Proof.
Print Pos.succ.
induction p.
  simpl.
Search (Pos.to_nat (_ ~0)).
rewrite Pos2Nat.inj_xO.
rewrite IHp.
rewrite Pos2Nat.inj_xI.
rewrite Nat.mul_succ_r.
rewrite !Nat.add_succ_r.
rewrite Nat.add_0_r.
easy.

cbn[Pos.succ].
rewrite Pos2Nat.inj_xI.
rewrite Pos2Nat.inj_xO.
easy.

easy.
Qed.

Fixpoint my_pred (p : positive) : positive :=
  match p with
  | xI p' => xO p'
  | xO xH => xH
  | xO p' => xI (my_pred p')
  | xH => xH
  end.

Lemma Pos_pred_correct :
  forall p, 2 <= Pos.to_nat p -> Pos.to_nat (my_pred p) = pred (Pos.to_nat p).
Proof.
intros p.
induction p as [p' Ih | p' Ih | ].
    cbn [my_pred].
    (* this all proof could be done by lia. --- package no. 1 *)
    intros _.
    destruct p' as [ q | q | ].
        rewrite Pos2Nat.inj_xO.
        rewrite !Pos2Nat.inj_xI.
        unfold Init.Nat.pred.
        easy.

      rewrite Pos2Nat.inj_xI.
      rewrite !Pos2Nat.inj_xO.
      easy.

    easy.
    (* end of package number 1. *)
  intros _.

  cbn[my_pred].
  destruct p' as [ q | q | ].
  (* this whole proof could be done by lia --- package no. 2 *)
      cbn [my_pred].
      rewrite !Pos2Nat.inj_xI.
      rewrite !Pos2Nat.inj_xO.
      rewrite !Pos2Nat.inj_xI.
      rewrite Nat.mul_succ_r, Nat.add_succ_r.
      unfold Init.Nat.pred.
      rewrite Nat.add_succ_r.
      rewrite Nat.add_0_r.
      easy.
   (*end of package 2. *)
    rewrite Pos2Nat.inj_xI.
    rewrite Ih.
    rewrite !Pos2Nat.inj_xO.
    assert (qgt0 : 0 < Pos.to_nat q) by apply Pos2Nat.is_pos.
    destruct (Pos.to_nat q).
      case (Nat.lt_irrefl 0); assumption.
      rewrite !Nat.mul_succ_r.
      rewrite !Nat.add_succ_r.
      rewrite !Nat.mul_succ_r.
      rewrite !Nat.add_succ_r.
      unfold pred.
      rewrite !Nat.mul_succ_r.
      rewrite !Nat.add_succ_r.
      rewrite !Nat.add_0_r.
      easy.
    rewrite !Pos2Nat.inj_xO.
    assert (qgt0 : 0 < Pos.to_nat q) by apply Pos2Nat.is_pos.
    destruct (Pos.to_nat q).
      case (Nat.lt_irrefl 0); assumption.
    rewrite !Nat.mul_succ_r.
    rewrite !Nat.add_succ_r.
    apply Peano.le_n_S.      
    apply Peano.le_n_S.      
    apply le_0_n.
  easy.
rewrite Pos2Nat.inj_1.
intros abs.
destruct (lt_not_le 1 2);[apply Nat.lt_succ_diag_r| assumption].
Qed.

Inductive btree {A : Type} : Type :=
| L (x : A) | N (t1 t2 : btree).

Check N (N (N (L 1) (L 2)) (N (L 3) (L 4)))
        (N (N (L 5) (L 6)) (N (L 7) (L 8))).


Fixpoint inst {A : Type} (e : A) (t : btree) :=
match t with
L x => N (L e) (L x)
| N t1 t2 => N (inst e t2) t1
end.

Fixpoint inslt {A : Type} (l : list A) (t : btree) :=
match l with
| e :: tl => inslt tl (inst e t)
| nil => t
end.

Compute inst 3 (N (L 1) (L 2)).

Compute inst 4 (N (N (L 3) (L 2)) (L 1)).

Compute seq 10 100.

Compute inslt (seq 10 100) (L 1).

Fixpoint insert (a : Z) (l : list Z) :=
  match l with
    nil => a::nil
  | b::tl =>
    if Zle_bool a b then a::b::tl else b::insert a tl
  end.

Fixpoint sort (l : list Z) :=
  match l with nil => nil | a::tl => insert a (sort tl) end.

Time Compute sort (List.rev (List.map Z.of_nat (seq 10 1000))).

Fixpoint merge_aux (a : Z) (l : list Z)
(f : list Z -> list Z) (l : list Z) :=
match l with
| b :: l' =>
if Zle_bool a b then f l else b :: merge_aux a l f l'
| nil => l
end.

Fixpoint merge (l1 : list Z) : list Z -> list Z:=
match l1 with
| a :: l1' =>
  fix m2 l2 : list Z :=
    match l2 with
    | b :: l2' =>
      if Zle_bool a b then a::merge l1' l2 else b::m2 l2'
    | nil => l1
    end
| nil => fun l2 => l2
end.

Fixpoint bintolist (t : btree) : list Z :=
match t with
L x => x::nil
| N t1 t2 => merge (bintolist t1) (bintolist t2)
end.

Definition msort l :=
match l with
  nil => nil
| a::tl => bintolist (inslt tl (L a))
end.

Time Compute msort (List.rev (List.map Z.of_nat (seq 10 1500))).
Time Compute sort (List.rev (List.map Z.of_nat (seq 10 1500))).

Inductive boundedZ (b : Z) : Type :=
  cbZ (x : Z) (h : (-1 < x < b)%Z).

Definition b3_10 : boundedZ 10.
exists 3%Z.
  lia.
Defined.

Check b3_10.

Definition mkboundedZ (b : Z) (bgood : (0 < b)%Z) (n : Z) : boundedZ b.
destruct (-1 <? n)%Z eqn:testm1.
  destruct (n <? b)%Z eqn:testb.
    exists n.
    split.
      rewrite <- Z.ltb_lt.
      assumption.
    rewrite <- Z.ltb_lt.   
    assumption.
  exists 0%Z.
    lia.
  exists 0%Z.
  lia.
Defined.

(*--------------(-1)------------a--------c---------b--------------U----- *)

Definition bz_to_Z {U : Z} (x : boundedZ U) :=
  match x with cbZ _ a h => a end.

Definition bounded_half_sum {U : Z} (a b : boundedZ U) : boundedZ U.
exists ((bz_to_Z a + bz_to_Z b) / 2)%Z.
assert (ub : (forall x, bz_to_Z (U := U) x < U)%Z).
  intros x; destruct x; simpl; tauto.
assert (lb : (forall x, 0 <= bz_to_Z (U:=U) x)%Z).
  intros x; destruct x.
    simpl; lia.
assert (a0 := lb a).
assert (b0 := lb b).
assert (a_u := ub a).
assert (b_u := ub b).
assert (0 <= (bz_to_Z a + bz_to_Z b) mod 2 <  2)%Z.
  apply Z.mod_pos_bound; lia.
split.
  apply (Zmult_lt_reg_r _ _ 2).
  lia.
  replace (-1 * 2)%Z with (-2)%Z by ring.
  rewrite Z.mul_comm.
  replace (2 * ((bz_to_Z a + bz_to_Z b) / 2))%Z with
    (bz_to_Z a + bz_to_Z b
          - (bz_to_Z a + bz_to_Z b) mod 2)%Z.
  lia.
  rewrite (Z_div_mod_eq (bz_to_Z a + bz_to_Z b) 2) at 1.
    ring.
  lia.
apply (Zmult_lt_reg_r _ _ 2).
  lia.
rewrite Z.mul_comm.
replace (2 * ((bz_to_Z a + bz_to_Z b) / 2))%Z with
 ((2 * ((bz_to_Z a + bz_to_Z b) / 2) + (bz_to_Z a + bz_to_Z b) mod 2) -
   (bz_to_Z a + bz_to_Z b) mod 2)%Z by ring.
  rewrite <- Z_div_mod_eq.
  lia.
lia.
Defined.

(* n positive  ---> to_nat --->  n natural ----> factorial
 * |                                                 |
 * |---> pfact ----> to_nat                          =     *)


Lemma Nat_iter_add {A : Type} (f : A -> A) (x : A) (n m : nat) :
  Nat.iter (n + m) f x = Nat.iter n f (Nat.iter m f x).
Proof.
induction n as [ | p Ih]; simpl; try rewrite Ih; auto.
Qed.

Lemma iter_pos_nat {A : Type} (f : A -> A) (x : A) p :
   Pos.iter f x p = Nat.iter (Pos.to_nat p) f x.
Proof.
revert x;
induction p as [ p Ih | p Ih | ]; intros x.
    cbn [Pos.iter Pos.to_nat].
    rewrite Pos2Nat.inj_xI.
    simpl; rewrite !Ih, !Nat_iter_add; simpl.
    easy.  
  cbn [Pos.iter Pos.to_nat].
  rewrite Pos2Nat.inj_xO.
  simpl; rewrite !Ih, !Nat_iter_add; simpl.
  easy.  
easy.
Qed.

Lemma Nat_iter_S {A : Type} (f : A -> A) (x : A) (n : nat) :
  Nat.iter (S n) f x = f (Nat.iter n f x).
Proof. easy. Qed.

Lemma Nat_iter_Sr {A : Type} (f : A -> A) (x : A) (n : nat) :
  Nat.iter (S n) f x = Nat.iter n f (f x).
Proof.
revert x.
induction n as [ | p Ih].
  easy.
simpl in Ih |- *.
now intros x; rewrite !Ih.
Qed.

Lemma pfact_correct :
  forall n, factorial (Pos.to_nat n) = Pos.to_nat (pfact n).
Proof.
unfold pfact.
intros n; rewrite iter_pos_nat.
enough (main: factorial (Pos.to_nat n) =
          Pos.to_nat (snd (Nat.iter (Pos.to_nat n)
           (fun '(x, y) => ((x + 1)%positive, (x * y)%positive))
           (1%positive, 1%positive)))  /\
           Pos.to_nat (fst (Nat.iter (Pos.to_nat n)
           (fun '(x, y) => ((x + 1)%positive, (x * y)%positive))
           (1%positive, 1%positive))) = S (Pos.to_nat n)).
destruct main as [m1 m2]; exact m1.
induction (Pos.to_nat n) as [ | n' [Ihf Ihi]].
  easy.
cbn [factorial].
rewrite Nat_iter_S; simpl.
destruct (Nat.iter n' _ _) as [i' f']; simpl in Ihf, Ihi |- *.
split.
  rewrite Pos2Nat.inj_mul.
  rewrite Ihf, Ihi.
  simpl.
  easy.
rewrite Pos2Nat.inj_add.
rewrite Ihi.
rewrite Pos2Nat.inj_1.
rewrite Nat.add_1_r.
easy.
Qed.
