Require Import Arith List Bool.

(* Question 9 *)
Fixpoint lsuc (ldigits : list nat) :=
  match ldigits with
  | nil => 1::nil
  | 9 :: tl => 0 :: lsuc tl
  | a :: tl => S a :: tl
  end.

Fixpoint nat_digits (n : nat) :=
  match n with
  | O => nil
  | S p => lsuc (nat_digits p)
  end.

Fixpoint value (ldigits : list nat) :=
  match ldigits with
  | nil => 0
  | a :: tl => a + 10 * value tl
  end.

Fixpoint licit (ldigits : list nat) :=
  match ldigits with
  | nil => true
  | a :: tl => (a <? 10) && licit tl
  end.

Fixpoint addl (l1 l2 : list nat) :=
  match l1, l2 with
  | nil, b => b
  | a :: tl1, nil => l1
  | a :: tl1, b :: tl2 =>
    if (a + b <? 10) then
      (a + b) :: addl tl1 tl2
    else
      (a + b - 10) :: lsuc (addl tl1 tl2)
  end.

Compute addl (7 :: 2 :: nil) (5 :: 3 :: nil).
Compute rev (addl (rev (9 :: 8 :: 3 :: nil)) (rev (1 :: 8 :: nil))).

Fixpoint small_mul (n : nat) (ldigits : list nat) :=
  match n with
  | 0 => nil
  | S p => addl ldigits (small_mul p ldigits)
  end.

(* question 14. *)
Fixpoint mull (l1 l2 : list nat) :=
  match l1 with
  | nil => nil
  | a::tl1 => addl (small_mul a l2) (0 :: mull tl1 l2)
  end.

Compute mull (9 :: 3 :: nil) (1 :: 4 :: nil).

(* A few more function: comparison, subtraction, division. *)
Fixpoint lzero (ldigits : list nat) :=
  match ldigits with
  | nil => true
  | a::tl => (a =? 0) && lzero tl
  end.

Fixpoint lcmp (l1 l2 : list nat) : comparison :=
  match l1, l2 with
  | nil, l2 => if lzero l2 then Eq else Lt
  | a :: tl1, b :: tl2 =>
    match lcmp tl1 tl2 with
    | Eq => (a ?= b)
    | Lt => Lt
    | Gt => Gt
    end
  | l1, nil => if lzero l1 then Eq else Gt
  end.

Definition lle (l1 l2 : list nat) : bool :=
  match lcmp l1 l2 with
  | Lt => true
  | Eq => true
  | Gt => false
  end.

Fixpoint small_div (k : nat) l q :=
match k with
| S k' => if lle (small_mul k q) l then k else small_div k' l q
| _ => 0
end.

Definition small_divide l q :=
  small_div 9 l q.

Fixpoint predl (ldigits : list nat) :=
  match ldigits with
  | nil => nil
  | 0 :: tl => 9 :: predl tl
  | S p :: tl => p :: tl
  end.
Fixpoint subl (l1 l2 : list nat) :=
  match l1, l2 with
  | a :: tl1, b :: tl2 =>
    if b <=? a then
      (a - b) :: subl tl1 tl2
    else
      (a + 10 - b) :: predl (subl tl1 tl2)
  | _, _ => l1
  end.


Fixpoint divl (input d : list nat) : list nat * list nat :=
match input with
  a :: l =>
    let (q, r) := divl l d in
    let n := small_divide (a::r) d in
      (n::q, subl (a::r) (mull (n::nil) d))
| nil => (nil, nil)
end.

Fixpoint powl (ldigits : list nat) (k : nat) :=
  match k with
  | 0 => 1 :: nil
  | S p => mull ldigits (powl ldigits p)
  end.

(* For the fun, the following functions and definitions give a computation
of the first 40 digits of PI, as a decimal representation.  It turns out
only the last 2 digits are inaccurate.
Instead of computing a rational number for each of the atan calls, we
compute an integer, which the integral part of the expected number multiplied
by some magnifier.

For PI, we take the magnifier corresponding to the number of decimals we
wish to compute.  For the calls to atan, we factor the multiplications by 4
in the magnifier. 

Here, because PI is such a well known number, we can verify that the
computation is "correct", at least for the most significant digits, but
for the least significant digits, we don't have a guarantee.  We would need
to make a precise mathematical proof to know how much we can trust the last
digits. *)

Fixpoint atan_mag_x (n : nat) (mag acc : list nat) p (y x : list nat) :=
  match n with
  | 0 => acc
  | S n' =>
    atan_mag_x n' mag
     (subl (addl acc (fst (divl mag (mull (addl p (2 :: nil)) 
                                     (mull y (powl x 2))))))
           (fst (divl mag (mull p y))))
        (addl p (4 :: nil)) (mull y (powl x 4)) x
  end.

Definition atanV mag n x :=
  atan_mag_x n mag (fst (divl mag x)) (3 :: nil) (powl x 3) x.

Definition PI_approx :=
  subl (atanV (mull (powl (0::1::nil) 40) (6::1::nil)) 27 (5 :: nil))
       (atanV (mull (powl (0::1::nil) 40) (4::nil)) 8 (9::3::2::nil)).

Fixpoint cleanup (ldigits : list nat) :=
  match ldigits with
  | 0 :: tl =>
    let tl' := cleanup tl in
      if lzero tl' then nil else 0 :: tl'
  | a :: tl => a :: cleanup tl
  | nil => nil
  end.

Compute rev (cleanup (PI_approx)).
