\documentclass{article}
\usepackage{url}
\usepackage{alltt}
\usepackage{color}
\usepackage{hhline}
\usepackage[latin1]{inputenc}
\title{Verifying programs and proofs\\
Part I. describe algorithms}
\author{Yves Bertot}
\date{January 2025}
\hfuzz=7pt
\begin{document}
\maketitle
\section{Motivating introduction}
Starting in the 1940s, it was understood that one could use computers to do math.  People who don't know much about mathematics usually think that the purpose of mathematics is to compute numbers and obviously computers can help compute.  But there is more in math than just computing numbers, there is also reasoning.

Reasoning follows rules, the rules of logic.  If you want to convince somebody, you need to follow these rules, otherwise the other side can always claim that they don't trust you.  The rules must be quite dumb, so that nobody can reject them, however stubborn they are.  The rules are so dumb, that actually a computer can check that they are applied correctly.

If Alice needs to check that Paul works well, it is very difficult for Alice if what Paul has to do is long and boring, so that Alice only has dumb rules to check.  But a computer can help, if it can recognize whether the rules are applied or not, because a computer never gets tired.

This is the game: reduce mathematics to something dumb, so that a computer can check it, and then rely on the computer.

Programming and software engineering can benefit from this, too.  To make sure that a program does what it is meant to do, we need to check all cases.  We can try to do it with massive testing, but testing can only catch mistakes somehow at random.  On the other hand, we can reason about what program do and verify that they behave correctly in all possible cases.

This approach raises a lot of interest in industry, especially for companies that are very vulnerable to software or hardware bugs.  For instance, Intel lost billions of dollars because the algorithm for division that they engraved in the Pentium (circa 1998) was faulty.  They hired specialists in computer-based proof (like the author of \cite{Harrison09}).  Aeronautics companies more and more replace physical objects (cables and rods) by networks and software in the command system of planes, but they have to convince certifying authorities that the software is as safe or safer than the physical devices they replace: they are interested in hiring software engineers that know how to guarantee the correctness of their software.  In France, the agency for research on nuclear energy also invests a lot of efforts on these approaches and the the national agency for software system security also publishes guidelines to use proof systems.

There are several approaches to verifying the quality of software.  Some approaches concentrate on programming languages that are closer to widespread langages like C or Java.  Other rely on programming languages that are easier to reason about in an efficient fashion.  We will use the latter approach and the language we consider is called a {\em functional programming language}.

\section{Starting the Coq system}
To work with Coq, you can start with three commands.  The first, called {\tt coqc} is used like a compiler;
the second, called {\tt coqtop} was initially meant to work in a terminal window: you type in commands and it prints out answers; the third one, based on {\tt visual studio code} with an extension called {\tt vscoq}, is an interactive development environment: it keeps a window where all your commands are recorded and other windows where the most recent results from the tool are displayed.  You are advised to use {\tt vscoq}.

  When using {\tt vscoq}, you must be aware that the commands you type are only processed if you explicitely require processing to happen, either by clicking on an arrow in the top row of the window or by typing in the appropriate combination of special characters (Alt-downarrow, Alt-rightarrow).  The tool tries to keeps a precise account of what you have done and how the state of the system has been modified, so that you are not allowed to modify a command that is recorded as ``processed'' (processed commands appear on different background).  When
you modify a processed command, all commands starting at the one you modify are
unprocessed.

These notes will describe only a subset of the features provided by the tool.  If you want to know more, or if something is not clear, you should look at the following pointer:
\begin{verbatim}
http://coq.inria.fr
\end{verbatim}
\section{First steps with the programming language}
The Coq system contains a programming language that is very simple but also quite unconventional.

For start, it is interesting to require that the system loads objects that have already been defined.  In our first experiments, we will use boolean values, integers, and lists.  So, we should start with the following command:
\begin{verbatim}
Require Import ZArith List Bool.
Open Scope Z_scope.
\end{verbatim}
Integers are already unconventional for people trained to use computers: they are unbounded.

In a programming language, you need to be able to define new things, you need to be able to test data and to take decisions based on the test results, and you need to be able to repeat operations.  This is what we will see in the following examples.

To define new things, you use the keyword {\tt Definition}.
\begin{verbatim}
Definition four := 4.

Definition polynom1 (x:Z) := x * x + 3 * x + 1.
\end{verbatim}
Types play an important role in Coq programming: most things have a type.  For instance, the numbers 0, 1, 2, \dots have type {\tt Z} in our setting.  When you write a function, like the {\tt polynom1} function above, you usually give the type of the expected arguments.  This is similar to conventional programming languages.

At any time you can use a command to verify if some formula is well formed.  This command is called {\tt Check}.
\begin{verbatim}
Check polynom1 (polynom1 (polynom1 (polynom1 (polynom1 4)))).
\end{verbatim}
This command performs no computation.  It only checks that you apply functions to arguments of the right type and it gives the type of the formula you gave
for checking.

To evaluate a value, you can use a command called {\tt Compute}.
\begin{verbatim}
Compute polynom1 (polynom1 4).
\end{verbatim}

In this programing language, we can also build functions without naming them.  Such functions only exist during the time we consider them.
\begin{verbatim}
Check (fun x => x * x, fun x => x * polynom1 x).
\end{verbatim}

We can also define things locally, so that they are forgotten after the command where they are supposed to be used.  The construct is called {\tt let}.

\begin{verbatim}
Check let x := 3 * 4 in x * x * x.
\end{verbatim}

You can also verify that a command will fail.  For instance, we know that {\tt x} is no longer defined after the previous command and we can verify that.
\begin{alltt}
Fail Check x.
\textcolor{blue}{The command has indeed failed with message:
=> Error: The reference x was not found in the current environment.}
\end{alltt}

\subsection{Simple data structures and pattern-matching}
In these first steps we will only use 5 different families of datatypes.

\subsubsection{Integers}
The first datatype will be the datatype of integers called {\tt Z}.

The datatype of integers comes with a few pre-defined functions and fairly usual notations.  For instance, we can compute the addition of two numbers:
\begin{alltt}
Compute 3 + 7.
\textcolor{blue}{ = 10 : Z}
\end{alltt}

There are also functions for comparing integers, with notation
{\tt <=?}, {\tt =?}, and {\tt <?},
the first computes whether the first number is less than or equal to the second number,
the second computes whether the two numbers are equal.  The result is a boolean value of type {\tt bool} that we describe more in details later.

\begin{alltt}
Compute 7 <=? 3.
\textcolor{blue}{ = false : bool}
\end{alltt}

Note that the notation for comparing two numbers in a program is particular, with the question mark attached to the end.  We shall later use a different notation for a different kind of comparison between numbers, which cannot be used inside programs.

\subsubsection{Lists}
The second family of datatypes that we will consider is the family of lists.  It is very similar to the arrays that we are accustomed to use in conventional programming language.  The common feature is that lists, like arrays, also have to contain elements that are all of the same type.  The difference is that you don't have to choose in advance the size of a list.

There are only two cases in lists: a list is either an empty list, with the internal name {\tt nil} or it is compound object with two sub-components, with the internal name {\tt cons}.  The first component is the first element of the list, the second component is another list that contains all the other elements of the list.

The internal name {\tt cons} is almost never used.  Instead we use a notation with ``{\tt{}::}''.  So we
can build a list with three elements as in the following command, but the answer of the computer uses the notation.
\begin{alltt}
Check cons 3 (cons 2 (cons 1 nil)).
\textcolor{blue}{3 :: 2 :: 1 :: nil
     : list Z}
\end{alltt}
Note that the type of the thing considered above is {\tt list Z}.  So when we consider a list,
the system always tries to know what the type of elements is.  We can also construct more complex
data, for instance lists of lists.  The following shows how to construct a list with two elements, which are both lists of integers.  This could be used to represent
a bi-dimensional array of 2\(\times\)2 elements.

\begin{alltt}
Check cons (cons 2 (cons 1 nil)) (cons (cons 3 (cons 2 nil)) nil).
\textcolor{blue}{(2 :: 1 :: nil) :: (3 :: 2 :: nil) :: nil
   : list (list Z)}
\end{alltt}

\subsubsection{pairs}
Lists can be used to group several things in one new thing, but they all have to be of the same type.  If you want to group several things together that are from different types, you will use
another datatype, called the type of pairs.  For instance, if you want to group together a number and a list of numbers, you will just write as follows:
\begin{alltt}
Check (3, 2::1::nil).
\textcolor{blue}{(3, 2::1::nil) : Z * list Z}
\end{alltt}
There is only one case in this datatype of pairs.

\subsubsection{boolean values}
In programming we often use a datatype that has only two elements, called {\tt true} and {\tt false}.  This datatype is called {\tt bool}.  The following command checks that {\tt true} really exists.

\begin{verbatim}
Check true.
\end{verbatim}

\subsubsection{optional values}
Sometimes, we will need to write a program that either returns exactly one value or returns an exceptional condition.  For instance, one may want to have a function that takes as input an integer and returns its square root if it is an
integer.  So this function would map 4 to 2 and 1 to 1.  But for the input 0, we want to express that there is no number that fits.  So we may choose that this function returns an element of the datatype {\tt option Z}.  This datatype has two cases, where the first case is called {\tt Some} and it has a subcomponent which is the thing we want to return.
The second case is called {\tt None} and it has no subcomponent.  So, for instance, to build an
element of {\tt option Z} we can write the following expression:
\begin{alltt}
Check Some 3.
\textcolor{blue}{Some 3 : option Z}
\end{alltt}

\subsection{Observing, testing, and making decisions}
If you manipulate an object in one of the datatypes, you may need to know in which case you are for the datatypes, and what are the subcomponents.  To do this, you use a construct called ``pattern matching''.

The first simple example is when the datatype has only one case.  Assume {\tt p} is a pair, you can write the following:
\begin{verbatim}
match p with
  (a, b) => ...
end
\end{verbatim}
inside the expression hidden in the dots, you are allowed to use {\tt a} to refer to the first component of {\tt p} and {\tt b} to refer to the second component.

For lists, it is a little more complex, because your program has to be ready to handle all possible cases in the datatype.  so you need to say what to do if {\tt p} is in the {\tt cons} case (noted with ``{\tt ::}") and what to do if {\tt p} is in the {\tt nil} case.
\begin{verbatim}
match p with
  a::v => ...
| nil =>  ...
end
\end{verbatim}
So this construct {\em tests} whether {\tt p} is empty or not, takes a decision accordingly and executes the piece of code that corresponds, using the variables {\tt a} and {\tt v} to observe the subcomponents.

If we use the pattern-matching construct on a boolean value, we do not observe subcomponents but we still take decisions.  This construct behaves exactly like an {\em if-then-else}.  This is illustrated by the notations of the Coq system.
\begin{alltt}
Check match 3 <=? 7 with true => 1 | false => 2 end.
\textcolor{blue}{if 3 <=? 7 then 1 else 2
     : Z}
\end{alltt}

We can illustrate the behavior of the pattern-matching construct using the {\tt Compute} command.  The first command computes the comparison between two integers and then takes a decision depending on the result.  The second command observes a list and returns its first element.  Note that the case where the list would be empty also needs to be covered in the {\tt match} construct.  This makes programming safer.
\begin{alltt}
Compute match 3 <=? 7 with true => 1 | false => 2 end.
\textcolor{blue}{ = 1 : Z}

Compute match 3::2::1::nil with nil => 0 | a::v => a end.
\textcolor{blue}{ = 3 : Z}

Compute match (3, true) with
  (a, b) => match b with false => 4 | true => 2 end
 end.
\textcolor{blue}{ = 2 : Z}
\end{alltt}

An alternative way to express pattern matching construct on pairs is available
with a {\tt let .. in } syntax:
\begin{alltt}
Compute let (a, b) = (3, true) in a.
\end{alltt}

\subsection{Defining and applying functions}
We can obtain functions in two ways.  Either functions were defined earlier and a name was given to them (by using {\tt Definition} or {\tt let}) or we build a function to use it directly.
To apply a function to an argument we simply write this function on the left of the argument.  All four of the following commands are correct.
\begin{alltt}
Check polynom1 1.
\textcolor{blue}{polynom1 1 : Z}

Check (fun x => x * x) 3.
\textcolor{blue}{(fun x => x * x) 3 : Z}

Check (fun x => (fun y => x + y)) 3.
\textcolor{blue}{(fun x => (fun y => x + y)) 3 : Z -> Z}

Check (fun x => (fun y => x + y)) 3 4.
\end{alltt}
In the last but one command, we see that the expression is a function, so it can be applied again to another number by writing it on the left of that number.  This is what we do in the last command.
Note that there is no need to use more parentheses.
\subsection{Repeating computation}
For repeating computation, the programming language of Coq is even more unconventional.  The important aspect is that Coq refuses to let you define functions that may loop forever.  Repetition is only allowed using recursion, and recursion is only allowed when the function's main argument gets smaller.  In our first steps with the Coq system, we will only consider recursive functions that take numbers and lists as arguments.

Here is a simple example of a function with recursion.  This function takes as input a list of
integers and adds all the elements in the list.
\begin{alltt}
Fixpoint sum_list_Z (l : list Z) :=
 match l with nil => 0 | a::tl => a + sum_list_Z tl end.

Compute sum_list_Z (1 :: 3 :: 4 :: nil).
\textcolor{blue}{ = 8 : Z}
\end{alltt}
First, note that the keyword for this definition is {\tt Fixpoint}.  You have to use this keyword if you want to define a function that has a repetitive behavior.  Second, note that the function relies on a pattern matching construct to decide on what to do.  In the case where the input list is empty, the computation terminates and return 0.  In the case where the input list contains a first number {\tt a} and a smaller list {\tt tl}, the computation of a recursive call of {\tt sum\_list\_Z} is required on this smaller list.  This will give a result to which {\tt a} will be added.  So, we explicitely have that the recursive call is done on a smaller list.

Recursive calls do not need to be done on direct subcomponents of the initial input.  For instance, the following function is accepted:
\begin{alltt}
Fixpoint teo (l : list Z) :=
  match l with
    nil => nil
  | a::nil => a::nil
  | a::b::tl => a::teo tl
  end.

Compute teo (1 :: 3 :: 4 :: nil).
\textcolor{blue}{ = 1 :: 4 :: nil : list Z}
\end{alltt}
The function {\tt teo} takes every other element of the input lists.  In the case where the input
list has the shape {\tt a::b::tl}, the recursive call is performed on the list {\tt tl} which is
visibly a subcomponent.

For recursive functions that have an integer as argument, a different structure
appears, because integers are not defined in one shot, but in several stages,
one of these stages relies on the binary encoding of binary numbers.

We can also define functions with several arguments, but the verification about recursive calls on smaller values must rely only on one of the argument positions.  The following example describes a function that adds all the elements of
a first list at the front of another list.
\begin{verbatim}
Fixpoint app_Z (l1 l2 : list Z) :=
  match l1 with
  | nil => l2
  | a :: tl => a :: app_Z tl l2
  end.
\end{verbatim}
In this function, the first argument decreases at each recursive call.  If we ask for the computation of {\tt app\_Z (2 :: 1 :: nil) (4 :: nil)}, then the first
recursive call requires that we compute
{\tt app\_Z (1 :: nil) (4 :: nil)}, a second recursive call requires that we
compute {\tt app\_Z nil (4 :: nil)}, This uses the first branch of the
pattern-matching construct and returns {\tt (4 :: nil)},
so that the first recursive call returns {\tt (1 :: 4 :: nil)} and the whole
expression computes to {\tt (2 :: 1 :: 4 :: nil)}.
\section{Transforming a complex algorithm into a functional program}
\label{sect:division}
One of the difficulties of programming in functional programming languages is to find ways to transform repetitive processes into recursive functions.  We will try to illustrate this using an algorithm that most of us know, because we learned it in elementary school: the division algorithm.  let's replay this algorithm on two examples: dividing
3845 by 17 and dividing 1645 by 17.

\begin{center}
\begin{tabular}{r|l c r | l}
3845 & 17 &\qquad &1645 & 17\\
\hhline{~|-~~|-}
\strut&\strut&&\strut&\strut\\
\end{tabular}
\end{center}

 Usually, we start by pushing the number 17 to the end of the dividends 3845 and 1645: because 17 only has two digits, we take the last two digits of the dividends, 38 for 3845 and 16 for 1645 and then we find the largest number \(a\) such \((a \times 17\)  is smaller than or equal to \(38\) or \(16\).  In the first case with \(a_1 = 2\)  because \(2 \times 17 = 34 <= 38 < 3\times 17 = 51\), in
the second case we find \(a_0 = 0\).  Then we compute the remainder by removing \(a\times 17\) the considered number.  In the first case we have \(38-2\times 17 = 4\), in the second case we keep \(16\).  We write \(a_1\) or \(a_2\) on the right part of our division figure as the beginning of our quotient: so the quotient start with \(2\) in the first case and with \(0\) in the second case (often, we don't bother writing this 0).

\begin{center}
\begin{tabular}{r|l c r | l}
{\tt3845} & {\tt17} &\qquad &{\tt1645} & {\tt17}\\
\hhline{~|-~~|-}
{\tt4~~~}&{\tt2}&&{\tt16~~~}&\\
\end{tabular}
\end{center}

The next step is to consider the number obtained by taking the first digit that was not yet considered in the dividend and stick it to the right of the remainder.  In our examples, the first digit is 4 and we stick it to the right of 4, which was the remainder in the first case, to obtain 44, and to the left of 16, which was the remainder in the second case, to obtain 164.

\begin{center}
\begin{tabular}{r|l c r | l}
{\tt3845} & {\tt17} &\qquad &{\tt1645} & {\tt17}\\
\hhline{~|-~~|-}
{\tt44~~}&2&&{\tt164~~}&\\
\end{tabular}
\end{center}

Now, we start repeating the same operations: we want to find the largest \(b\) such that \(b\times 17\) is smaller than or equal to \(44\) in the first case and smaller than or equal to \(164\) in the second case.  We obtain \(b_1 = 2\) in the first case and \(b_2=9\) in the second case, and the new remainders are \(10\) and \(164-9\times17=164-153=11\) in the second case.  The quotient becomes \(22\) in the first case and \(9\) in the second case.

\begin{center}
\begin{tabular}{r|l c r | l}
{\tt3845} & {\tt17} &\qquad &{\tt1645} & {\tt17}\\
\hhline{~|-~~|-}
{\tt44~~}&22&&{\tt164~~}&9\\
{\tt10~~}&&&{\tt11~~}\\
\end{tabular}
\end{center}

Then we repeat the same steps again and by taking again the first digit that was not yet considered in the dividend and add it to the left of the remainder.  So, we take 5 and stick it to \(10\) to obtain \(105\) in the first case, and to \(11\) to obtain \(115\) in the second case.
\begin{center}
\begin{tabular}{r|l c r | l}
{\tt3845} & {\tt17} &\qquad &{\tt1645} & {\tt17}\\
\hhline{~|-~~|-}
{\tt44~~}&22&&{\tt164~~}&9\\
{\tt105~}&&&{\tt115~}\\
\end{tabular}
\end{center}

Division by 17 yields a digit that is 6 in both cases, so the new quotient
is \(226\) in the first case, with a remainder of 3, and \(96\) in the second case, with a remainder of 13.
\begin{center}
\begin{tabular}{r|l c r | l}
{\tt3845} & {\tt17} &\qquad &{\tt1645} & {\tt17}\\
\hhline{~|-~~|-}
{\tt44~~}&226&&{\tt164~~}&96\\
{\tt105~}&&&{\tt115~}&\\
{\tt3~}&&&{\tt13~}&
\end{tabular}
\end{center}

 We can check that the result is correct by computing the following equations:
\[226 \times 17 + 3 = 3845 \qquad 96 \times 17 + 3 = 1645.\]

Now, let's work on representing this algorithm in the Coq system.  This algorithm works with numbers as if they were sequences of digits, so we will use lists of digits to represent the numbers.  In this algorithm, we add digits to the right (for instance, we stick 4 to the right of 16 to obtain 164) but in a list, we add elements to the left, so we will use the convention of representing a number by a list of digits where the digits come in reverse order.  To represent the digits, we will simply use integers.  So for instance, the number 239 will be represented by the list (9::3::2::nil).

To understand how to represent the algorithm recursively, we need to look at one of the last repetitions of the algorithm.  What is happening there, for instance when dividing
3845 by 17, we look specifically at 5, and we use the remainder of dividing 384 by 17, which is 10, and the quotient of that division, which is 22.  So we can say:  We take the list {\tt (a::l)} (where {\tt a} is 5 in the example and {\tt l} is {\tt (4::8::3::nil)}) and we divide {\tt l} by the divisor
(let's call it {\tt d}), thus obtaining a quotient q and a remainder r.  So in our presentation of the algorithm we will write something like that:

\begin{verbatim}
match input with
  a::l => let (q, r) := divide l d in
\end{verbatim}
The next step is to stick the number 5 to 10, so with our variables, we stick {\tt a} on the left of {\tt r}, which brings us to consider the list {\tt a::r} and to perform a ``small division'' on this list: we know that this small division will return only one digit, that is
a number smaller than 9, which will be represented as an integer.  So, for now, we assume that there exists another function to perform the small division, which returns just the required number.  Let's call this small division function {\tt small\_divide}, we
want to compute the following expression:
\begin{verbatim}
let n := small_divide (a::r) d in
\end{verbatim}
Thus we give the name {\tt n} to the result of the small division.  Now we want to use this name {\tt n} in two ways.  First we want to stick {\tt n} to the left of the previous quotient {\tt q} to obtain the new quotient, second we want to remove {\tt n * d} from {\tt (a::r)} to obtain the new remainder (in the example, this corresponds to computing \(105 - 6\times 17=3\).  For this task, we assume we have two function subtract and multiply that act on lists of digits to represent the arithmetic operations.  So we complete the work by writing the value that is returned:
\begin{verbatim}
  (n::q, subtract (a::r) (multiply (n::nil) d))
\end{verbatim}
In all, the recursive case of the algorithm is written as:
\begin{verbatim}
match input with
  a::l => let (q, r) := divide l d in
  let n := small_divide (a::r) d in
  (n::q, subtract (a::r) (multiply (n::nil) d))
\end{verbatim}
Now, we have to consider the other case for the dividend.  This other case is when the number is represented by the empty list.  What number is represented by the empty list?
Actually, it is 0.  So if the list is empty, we want to divide 0 by a given divisor {\tt d}, the obvious result is the quotient 0 and the remainder 0, so we can simply return the following value, where {\tt nil} is used to represent 0 in the remainder part of the result.
\begin{verbatim}
  (0, nil)
\end{verbatim}
In the end, the whole division algorithm is described as follows:
\begin{verbatim}
Fixpoint divide (input d : list Z) : list Z * list Z :=
  match input with
    a :: l =>
    let (q, r) := divide l d in
    let n := small_divide (a::r) d in
    (n::q, subtract (a::r) (multiply (n::nil) d))
  | nil => (nil, nil)
  end.
\end{verbatim}
Note that this recursive function has a recusive call on {\tt l} as first argument, which is a sub-component of {\tt input} as decribed by the pattern-matching construct.

It remains to describe the functions that are being used in the algorithm.  Let's look at {\tt small\_divide}.  We know we want to find a number that is smaller than 10, so we can perform a little exhaustive search to find the correct one, starting at 9.  This can be written as follows:
\begin{verbatim}
Fixpoint small_div (ks : list Z) l q :=
  match ks with
  | k :: tl =>
    if less_than (n_l_mul k q) l then k else small_div tl l q
  | _ => 0
  end.

Definition small_divide l q :=
  small_div (9 :: 8 :: 7 :: 6 :: 5 :: 4 :: 3 :: 2 :: 1 :: nil) l q.
\end{verbatim}
Again, we have to define a new auxiliary function, {\tt less\_than}, which checks whether
the number represented by a list of digits is strictly smaller than another.  Here is an example implementation:
\begin{verbatim}
Fixpoint is_zero (l : list Z) :=
  match l with
  | a :: tl => (a =? 0) && is_zero tl
  | nil => true
  end.

Fixpoint eq_number (l1 l2 : list Z) :=
  match l1, l2 with
  | a::t1, b::t2 => (a =? b) && eq_number t1 t2
  | _, _ => is_zero l1 && is_zero l2
  end.

Fixpoint less_than (l1 l2 : list Z) :=
  match l1, l2 with
  | a :: tl1, b :: tl2 =>
    if eq_number tl1 tl2 then a <=? b else less_than tl1 tl2
  | _, b :: tl2 => true
  | _, _ => is_zero l1
  end.
\end{verbatim}
\section{A few predefined functions for use in exercises}
Later in the course, we will show how to define the following functions, but
for now it enough to know that these functions exist and to use them.

\subsection{Repeating a function a number of times}
In this course, we did not explain how to program recursively with integers,
because it is not as primitive as recursive programming with lists.  However,
many of the functions that one may want to write can defined using the
function {\tt Z.iter}, which repeats a given function a number of times.

Here is an example where we use {\tt Z.iter} to define the function that
returns the sequence of \(n\) numbers \(0, \ldots, (n - 1)\) when \(n\) is
non-negative and the empty list if \(n\) is negative.

\begin{verbatim}
Definition Zseq (n : Z) : list Z :=
  if (n <=? 0) then [] else
  snd (Z.iter n (fun (x, l) => (x + 1, app_Z l (x :: nil))) (0, nil)).
\end{verbatim}
\subsection {Applying a function to all elements of a list}
The predefined function to apply a function to all elements of a list is
called {\tt map}.
For instance, {\tt map (fun x => x + 1) (Zseq 10)} will return the list of
numbers from 1 to 10.
\subsection{Computing the list of digits (in base 10)}
The type {\tt Z} provides a function {\tt Z.log2\_up} which computes the
logarithm in base 2 of an integer, rounded above to an integer.  Because 2 is
smaller than 10, {\tt Z.log2\_up x + 1} is an upper bound of the number of
digits in the decimal representation of a number.  We can then use this to
compute the number of digits of a number in logarithmic time.
\begin{verbatim}
Definition Z_to_digits (x : Z) : list Z :=
  if x <=? 0 then
	  []
	else
    snd (Z.iter (1 + (Z.log2_up x))
         (fun '(x, l) =>
             if (x =? 0) then (x, l) else (x / 10, l ++ [x mod 10]))
	 (x, [])).
\end{verbatim}
\section{Exercises}
\begin{enumerate}
\item Define a function that takes as input a list of integers and returns the product of these integers,
\item Define a function that takes a list of integers and returns {\tt true} if and only if
this list contains the integer {\tt 0}.
\item Define a function that takes an integer {\tt n} and an integer {\tt a} as input and returns a list of integers containing {\tt n} elements that are all {\tt a}.
\item Define a function that takes an integer {\tt n} and an integer {\tt a} and returns
the list of {\tt n} elements \(a {\tt::} a+1 {\tt::} \cdots {\tt::} a + (n - 1) {\tt ::nil}\).
\item Define a function that takes as input an integer and returns an element of {\tt option Z} containing the predecessor if it is not negative or {\tt None} otherwise.
\item Define a function that takes as input a list of integers and returns the length of this list.
\item Can you write a function {\tt values} that takes as input a function \(f\) of type {\tt Z -> Z}, an initial value \(a\) of type {\tt Z} and a count \(n\) of type {\tt Z} and produces the list
\begin{alltt}
\(a\) :: \(f\) \(a\) :: \(f\) (\(f\) \(a\)) :: ...
\end{alltt}
\end{enumerate}
\section{More information}
You can use the book \cite{coqart} (available in French on internet, otherwise you should find English versions at the library) and
the reference manual \cite{coqmanual}.  There is also a tutorial in French \cite{surviecoq}.
There are also tutorials on the web \cite{coqhurry,tutreccoq}.
\begin{thebibliography}{10}
\bibitem{coqart}
Y.~Bertot and P.~Cast{\'e}ran.
\newblock {\em Interactive Theorem Proving and Program Development, Coq'Art:the
  Calculus of Inductive Constructions}.
\newblock Springer-Verlag, 2004.
\bibitem{softwarefoundations}
B.~Pierce et al.
\newblock{\em Software Foundations}
\newblock{\url{http://www.cis.upenn.edu/~bcpierce/sf/}}
\bibitem{coqhurry}
Y.~Bertot
\newblock{\em Coq in a Hurry}
\newblock Archive ouverte ``cours en ligne'',  2008.
\newblock {\url{http://cel.archives-ouvertes.fr/inria-00001173}}
\bibitem{coqmanual}
The Coq development team.
\newblock {\em The Coq proof Assistant Reference Manual},
\newblock Ecole Polytechnique, INRIA, Université de Paris-Sud, 2004.
\newblock {\url{http://coq.inria.fr/doc/main.html}}
\bibitem{Harrison09}
John Harrison,
\newblock{\em Handbook of Practical Logic and Automated Reasoning'}
\newblock Cambridge University Press, 2009
\newblock{\url{http://www.cl.cam.ac.uk/~jrh13/atp/}}
\bibitem{tutreccoq}
E.~Giménez, P.~Castéran,
\newblock{\em A Tutorial on Recursive Types in Coq},
\newblock INRIA, Université de Bordeaux, 2006.
\newblock{\url{http://www.labri.fr/Perso/~casteran/RecTutorial.pdf.gz}}
\bibitem{surviecoq}
A.~Miquel,
\newblock{\em Petit guide de survie en Coq},
\newblock{Université de Paris VII}.
\newblock{\url{http://www.pps.jussieu.fr/~miquel/enseignement/mpri/guide.html}}
\end{thebibliography}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
