Require Import ZArith Lia.

Open Scope Z_scope.

Check Z.opp.
Check 2.
Check Z.opp 2.

Section sandbox.
Variable F : Z -> Type.

Variable g : forall x : Z, F x.

Check g.
Check 2.
Check g 2.
End sandbox.

Check le_n.

Check le_S.

Check le_n 3.

Check le_S 3 3 (le_n 3).

Record upper_bounded (k : Z) :=
   mkub { toZ : Z; ubP : toZ < k }.

Check mkub.

Lemma Zm1lt x : x - 1 < x. Proof. lia. Qed.

Definition maxub (k : Z) : upper_bounded k :=
  mkub k (k - 1) (Zm1lt k).

Check maxub.

Check maxub 3.

Arguments toZ [k].

Compute toZ (maxub 3).

Definition bool_dep (P : bool -> Type)
  (ft : P true) (ff : P false)
  (b : bool) : P b :=
  match b with | true => ft | false => ff end.

Check bool_dep.

Definition Zltb_to_lt x y : x <? y = true -> x < y :=
   proj1 (Z.ltb_lt x y).

Definition below_bound_case (k x : Z) :
   x <? k = true -> upper_bounded k :=
  fun h : x <? k = true =>
     mkub k x (Zltb_to_lt x k h).

Definition above_bound_case (k x : Z) :
   x <? k = false -> upper_bounded k :=
   fun h => maxub k.

Check fun k x =>
  bool_dep (fun b => x <? k = b -> upper_bounded k)
    (below_bound_case k x)
    (above_bound_case k x).

Check fun k x =>
  bool_dep (fun b => x <? k = b -> upper_bounded k)
    (below_bound_case k x) (above_bound_case k x)
    (x <? k).

Check fun k x =>
  bool_dep (fun b => x <? k = b -> upper_bounded k)
    (below_bound_case k x) (above_bound_case k x)
    (x <? k) refl_equal.

Definition test_mkub k x : upper_bounded k :=
  bool_dep (fun b => x <? k = b -> upper_bounded k)
    (below_bound_case k x) (above_bound_case k x)
    (x <? k) refl_equal.

Check test_mkub 10 3.

Compute toZ (test_mkub 10 3).

Definition test_mkub2 (k n : Z) : upper_bounded k.
destruct (n <? k) eqn:h.
  apply (mkub k n).
  apply Z.ltb_lt.
  exact h.
apply maxub.
Defined.

Compute toZ (test_mkub2 10 3).

Definition witness {A : Type}(a : A) :
   {b : A | a = b} :=
  exist (fun b' => a = b') a eq_refl.

Definition test_mkub3 (k x : Z) : upper_bounded k :=
  match witness (x <? k) with
  | exist _ true h =>
    (* here h is a proof that x <? k = true *)
    mkub k x (proj1 (Z.ltb_lt x k) h)
  | exist _ false _ => maxub k
  end.

Compute toZ (test_mkub3 10 3).

Definition bool_dep' (P : bool -> Type) : P true -> P false -> forall b, P b.
intros ht hf b.
destruct b.
  exact ht.
exact hf.
Defined.

Definition nat_dep_case (P : nat -> Prop) (h0 : P O)
  (hs : forall k, P (S k)) (n : nat) : P n :=
  match n with
  | O => h0
  | S p => hs p
  end.

Fixpoint nat_dep_rec (P : nat -> Prop) (h0 : P O)
  (hs : forall k, P k -> P (S k)) (n : nat) : P n :=
  match n with
  | O => h0
  | S p => hs p (nat_dep_rec P h0 hs p)
  end.

Inductive singleton3 : Z -> Prop :=
  is_3 : singleton3 3.

Definition singleton3_dep (x : Z) (prf : singleton3 x) 
  (P : Z -> Prop) (p3 : P 3) : P x :=
  match prf with is_3 => p3 end.

Inductive singleton {A : Type} (x : A) : A -> Prop :=
  single : singleton x x.

Definition singleton_dep
   {A : Type} {x y : A} (prf : singleton x y)
   (P : A -> Prop) (px : P x)  : P y :=
  match prf with single _ => px end.  

Check single.

Check singleton_dep.

Lemma singleton_eq {A : Type} (x y : A) : singleton x y <-> x = y.
Proof.
split.
  intros sg.
  apply (singleton_dep sg).
  reflexivity.
intros xqy.
rewrite xqy.
apply single.
Qed.

Print eq.
Print eq_ind.
