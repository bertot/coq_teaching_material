Require Import Arith.

Search "even" nat.

(* Exercise 1. *)

Definition multiple (a b : nat) : Prop :=
  exists k, a = k * b.

(* Exercise 2. *)
Check (forall n: nat, multiple n 2 -> multiple (n * n) 2).

(* Exercise 3. *)
Check (forall k n, multiple n k -> multiple (n * n) k).

(* Exercise 4. first take. *)
Definition odd n := not (multiple n 2).

(* Exercise 4. second take. *)
Definition odd' n := exists k, n = 2 * k + 1.

(* Exercise 5. *)
Section playground_for_exercise5.

(* Assumptions of the question. *)
Variables (T : Type) (nat_of_T : T -> nat) (tadd : T -> T -> T).

(* Answer to the question. *)

Check forall x y : T, nat_of_T (tadd x y) = nat_of_T x + nat_of_T y.

End playground_for_exercise5.

(* Exercise 6. *)

Lemma exe6 :
  forall P Q : nat -> Prop, 
  forall x y : nat, (forall z, P z -> Q z) -> x = y -> P x ->
  P x /\ Q y.
Proof.
intros P Q x y PQ xy Px.
split.
  exact Px.
apply PQ.
rewrite <- xy.
exact Px.
Qed.

(* Exercise 7. *)
Lemma exe7 :
  forall A B C : Prop, (A /\ B) \/ C -> A \/ C.
Proof.
intros A B C tmp.
destruct tmp as [hAnB | hC].
  left.
  destruct hAnB as [hA hB].
  exact hA.
right.
exact hC.
Qed.

(* Exercise 8. *)
Lemma exe8 :
  forall P : nat -> Prop, (forall x, P x) ->
  exists y:nat, P y /\ y = 0.
Proof.
intros P aP.
exists 0.
split.
  apply aP.
reflexivity.
Qed.

(* Exercise 9. *)

Lemma multiple_square k n : multiple n k -> multiple (n * n) k.
Proof.
intros [d dP].
exists (n * d).
rewrite dP; ring.
Qed.

Lemma multiple_square' k n : multiple n k -> multiple (n * n) k.
Proof.
intros [d dP].
exists (n * d).
rewrite dP.
rewrite Nat.mul_assoc.
reflexivity.
Qed.

(* Exercise 10. First definition of odd. *)

Lemma odd_square n : odd n -> odd (n * n).
Proof.
assert (main: (odd n -> odd (n * n)) /\ (odd (S n) -> odd (S n * S n))).
induction n as [ | p IH].
split.
  intros abs; case abs; exists 0; reflexivity.
rewrite Nat.mul_1_l; auto.
split.
  destruct IH as [_ it]; exact it.
  intros osp abs.
  assert (o_p : odd p).
    intros [k kP]; case osp.
    exists (k + 1); rewrite kP; ring.
  destruct abs as [k kP].
    assert (multiple (p * p) 2).
    exists (k - 2 * p - 2).
    rewrite !Nat.mul_sub_distr_r.
    rewrite <- kP.
    replace (S (S p) * S (S p)) with (p * p + 2 * 2 + 2 * p * 2) by ring.
    rewrite !Nat.add_sub.
    easy.
  now destruct IH as [IHl _]; case (IHl o_p).
tauto.
Qed.

(* Exercise 10. second definition of odd. *)

Lemma odd'_square n : odd' n -> odd' (n * n).
Proof.
intros [k kP]; exists (n * k + k); rewrite kP; ring.
Qed.


