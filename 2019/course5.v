Require Import ZArith Arith List Lia.

Import ListNotations.

Print positive.
Check xO (xI (xO xH)).

Fixpoint pos_to_nat (x : positive) : nat :=
  match x with
    xH => 1
  | xO p => 2 * pos_to_nat p
  | xI p => S (2 * pos_to_nat p)
  end.

Fixpoint add1 (x : positive) : positive :=
  match x with
    xH => xO xH
  | xO p => xI p
  | xI p => xO (add1 p)
  end.

Fixpoint pos_add (x y : positive) (c : bool) : positive :=
 match x, y, c with
   xI x', xI y', false => xO (pos_add x' y' true)
 | xO x', xI y', false => xI (pos_add x' y' false)
 | xI x', xO y', false => xI (pos_add x' y' false)
 | xO x', xO y', false => xO (pos_add x' y' false)
 | xH, y, false => add1 y
 | x, xH, false => add1 x
 | xI x', xI y', true => xI (pos_add x' y' true)
 | xO x', xI y', true => xO (pos_add x' y' true)
 | xI x', xO y', true => xO (pos_add x' y' true)
 | xO x', xO y', true => xI (pos_add x' y' false)
 | xH, xH, true => xI xH
 | xH, xI y, true => xI (add1 y)
 | xH, xO y, true => xO (add1 y)
 | xI x, xH, true => xI (add1 x)
 | xO x, xH, true => xO (add1 x)
end.

Lemma add1_correct : forall x, pos_to_nat (add1 x) = S (pos_to_nat x).
Proof.
induction x.
simpl.
rewrite IHx; ring.
simpl.
reflexivity.
reflexivity.
Qed.

Lemma pos_add_correct :
  forall x y c, pos_to_nat (pos_add x y c) =
    pos_to_nat x + pos_to_nat y + if c then 1 else 0.
Proof.
induction x as [x' | x' | ]; intros [y' | y' | ] [ | ]; simpl; 
  try rewrite add1_correct; try rewrite IHx'; try ring.
Qed.

Fixpoint insert (a : Z) (l : list Z) :=
  match l with
    nil => a::nil
  | b::tl => if Zle_bool a b then a::b::tl else b::insert a tl
  end.

Fixpoint sort (l : list Z) :=
  match l with nil => nil | a::tl => insert a (sort tl) end.

Fixpoint count (x : Z) (l : list Z) :=
  match l with
    nil => 0
  | y::tl =>
    if (x =? y)%Z then 1 + count x tl else count x tl
  end.

Fixpoint sorted (l : list Z) :=
  match l with
    a :: (b :: tl) as l' =>
    if Zle_bool a b then sorted l' else false
  | _ => true
  end.

Check (refl_equal : sorted [2 ; 500; 4; 8]%Z = false).

Lemma sort_perm : forall x l, count x l = count x (sort l).
Proof.
assert (insert_perm : forall x a l, count x (insert a l) = count x (a :: l)).
  intros x a; induction l as [ | b l Ih].
    easy.
  simpl.
  case (a <=? b)%Z eqn:cmp.
    case (x =? a)%Z eqn: testa.
      case (x =? b)%Z eqn: testb.
        rewrite Z.eqb_eq in testa; rewrite Z.eqb_eq in testb.
        rewrite <- testb, testa; simpl.
        now rewrite Z.eqb_refl.
     rewrite Z.eqb_eq in testa.
     now rewrite <- testa; simpl; rewrite Z.eqb_refl, testb.
   case (x =? b)%Z eqn: testb.
     rewrite Z.eqb_eq in testb; rewrite <- testb; simpl.
     now rewrite testa, Z.eqb_refl.
   now simpl; rewrite testa, testb.
case (x =? a)%Z eqn: testa.
  case (x =? b)%Z eqn: testb.
    rewrite Z.eqb_eq in testa; rewrite Z.eqb_eq in testb.
    rewrite <- testb, testa; simpl.
    rewrite Z.eqb_refl.
    now rewrite testa in Ih; rewrite Ih; simpl; rewrite Z.eqb_refl.
  rewrite Z.eqb_eq in testa.
  rewrite <- testa; simpl; rewrite testb.
  now rewrite testa in Ih |- *; rewrite Ih; simpl; rewrite Z.eqb_refl.
  case (x =? b)%Z eqn: testb.
   rewrite Z.eqb_eq in testb; rewrite <- testb; simpl.
     now rewrite Z.eqb_refl, Ih; simpl; rewrite testa.
   now simpl; rewrite testb, Ih; simpl; rewrite testa.
intros x l; induction l as [ | a l Ih].
  easy.
simpl.
destruct (x =? a)%Z eqn: testa.
  now rewrite insert_perm; simpl; rewrite testa, Ih.
now rewrite insert_perm; simpl; rewrite testa.
Qed.

Lemma insert_head a l  def : 
  (l <> nil -> hd def (insert a l) = a \/ hd def (insert a l) = hd def l).
Proof.
intros lnnil.
destruct l as [ | b l]; auto.
now simpl; destruct (a <=? b)%Z; auto.
Qed.

Lemma insert_sorted a l : sorted l = true -> sorted (insert a l) = true.
Proof.
induction l as [ | b l Ih]; auto.
simpl.
destruct (a <=? b)%Z eqn: cmp.
  destruct l as [ | c l].
     now (simpl; rewrite cmp).
  destruct (b <=? c)%Z eqn: cmp2.
    change (sorted (a :: b :: c :: l)) with
    (if (a <=? b)%Z then if (b <=? c)%Z then
         sorted (c :: l) else false else false).
    now rewrite cmp, cmp2.
  discriminate.
destruct l as [ | c l].
  simpl.
  rewrite Z.leb_nle in cmp.
  assert (cmp' : (b <= a)%Z) by lia.
  rewrite <- Z.leb_le in cmp'.
  now rewrite cmp'.
destruct (b <=? c)%Z eqn: cmp2; try easy.
destruct (insert a (c :: l)) as [ | d l'] eqn:ins'.
  simpl in ins'; destruct l; easy.
intros rec; apply Ih in rec.
assert (cmp3 : (b <= d)%Z).
  rewrite Z.leb_le in cmp2; rewrite Z.leb_nle in cmp.
  assert (nnil : c :: l <> nil) by discriminate.
  assert (ihead := insert_head a (c :: l) a nnil); rewrite ins' in ihead.
  simpl in ihead.
  lia.
replace (sorted (b :: d :: l')) with 
  (if (b <=? d)%Z then sorted (d :: l') else false) by easy.
now rewrite <- Z.leb_le in cmp3; rewrite cmp3.
Qed.

Lemma sort_sorted : forall l, sorted (sort l) = true.
Proof.
intros l; induction l as [ | a l Ih].
  easy.
now simpl; apply insert_sorted.
Qed.
