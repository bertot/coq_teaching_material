\documentclass{article}
%\usepackage{a4wide}
\usepackage{geometry}
\usepackage{url}
\usepackage{alltt}
\usepackage{color}
\usepackage[latin1]{inputenc}

\geometry{
  includeheadfoot,
  margin=2.54cm
}

\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqnot}{\mbox{\~{}}}
\newcommand{\coqor}{{\tt\char'134/}}
\title{Verifying programs and proofs\\
part II. describe program properties}
\author{Yves Bertot}
\date{January 2025}
\begin{document}
\maketitle
\section{Motivating introduction}
While the motivation of the proof system is to avoid bugs in functional
programs, it is amazing that this system does not provide any advance tool
to step through the execution of programs.

On the other hand, it provides ways to express logical relations
between various pieces of data.  For instance, we can express logically the
relation that should exist between a function output and its input, in a way
that is independent from the actual algorithm implemented in the
function.  If we think about a division function (as we learn it in
school), there is a simple relation between the divided number {\tt
  x}, the divisor {\tt y}, the computed quotient {\tt q}, and the
computed remainder {\tt r}:

\begin{verbatim}
x = y * q + r /\ 0 <= r < y
\end{verbatim}
In English, this relation states that {\tt x} is equal to the sum of
the product of {\tt y} and {\tt q} and {\tt r} and that {\tt r} is
between {\tt 0} and {\tt y}.  This relation should be satisfied by the pair
of numbers returned by a division algorithms, but this can only be
satisfied if we avoid dividing by 0.  So the logical statement expressing
that division is implemented correctly should both express this expected
outcome and this restriction on the inputs.

In this course, we will concentrate on the language used to express
relations between pieces of data.

We will learn how to write logical formulas that
represent correctly what we want to express and how to prove these
formulas.  This may be difficult for readers that have little
acquaintances with logic, but we should try to learn by
practice.  We know that we understand it when we start being able to
write concise formulas that we can prove, thus getting lemmas, and
when we can use these lemmas to prove other formulas, thus getting new
theorems.

\section{Writing logical formulas}
\subsection{Prerequisites}
To run the examples that follow with the Coq system, we need to place ourselves
in the right context.  This is done by telling the proof system to load
predefined libraries for integers, lists, boolean values.

\begin{verbatim}
Require Import ZArith List Bool.

Open Scope Z_scope.
\end{verbatim}

For the exercises, the author of these notes also prepared some sets of
predefined functions which can be loaded in this manner.  If you use the
docker image {\tt ybertot/course2025}, then you only have to type the
following command inside Coq to get access to these predefined functions.
\begin{verbatim}
From YBCourse2025 Require Import predefined_functions.
\end{verbatim}


\subsection{Predicates}
In this section, we will consider a new type, the type of propositions
and ways to build atomic objects in this type.  The simplest way to
assert a proposition is to say that some value is equal to another.
Here two examples:

\begin{alltt}
Check 3 = 2 + 1.
\textcolor{blue}{3 = 2 + 1 : Prop}

Check 3 = 4.
\textcolor{blue}{3 = 4 : Prop}
\end{alltt}
An equality is well-formed when the left-hand-side and the right-hand-side
have the same type.

Note that you can always write a logical proposition, as long as it is
well-formed, even if this proposition is false (here {\tt 3 = 4} is a
well-formed logical proposition, even if it is false).  The true and false
propositions are not distinguished by the fact that they can be written, but
by the fact that they can be proved.

When it comes to numbers, the Coq system also provides us with other
predicates that correspond to comparisons : {\tt 3 <= 4}, {3 < 4}, and the
symmetric relations.  To verify algorithms about cryptography, it may also
be useful to know when a number is prime, for instance.

In this course, we will define new predicates by simply inventing new
functions of one or two arguments that return a value that is then
compared to another value.  For instance, we can say that a number
is even if that number modulo 2 is equal to 0.
\begin{alltt}
Definition even (x : Z) := x mod 2 = 0.

Check 3 mod 2 =? 0 : bool.
\textcolor{blue}{3 mod 2 =? 0 : bool}

Check even 3.
\textcolor{blue}{even 3 : Prop}
\end{alltt}

We see here that there is a distinction between boolean values and
propositions.  In practice, boolean values are meant to be the result
of tests that can be checked by a program, and these tests can be used in
other program.  On the other hand, propositions may
represent statements that no algorithm can verify in one run.  Boolean
values can be produced and tested in programs.  Logical proposition do
not belong inside programs, at least for our lessons.

\subsection{Examples with lists}
Lists manipulations are a nice source of exercises for recursive programming
and verifying algorithms.  We will exercise a small collection of functions.

\begin{enumerate}
\item A function {\tt Zlength} is already provided in the {\tt List} library.
It takes an argument that is a list of elements in any type and returns
the length of that list, an integer.
\item The predicate {\tt In} is already provided in the {\tt List} library.  It
takes two arguments, an element \(x\) in any type and a list \(l\)
of elements of that type and it holds when \(x\) appears among the elements
of that list.
\item The function {\tt Znth} is a function designed specifically for this lesson.
It takes as input a list of elements in an arbitrary type and an integer \(n\),
and it returns the element at position \(n\) in that list, starting count at
0.  However, there may be reasons for this to fail, because \(n\) is negative
or larger than the size of the list, no value can be found in the list.
\begin{verbatim}
Fixpoint Znth {A : Type}(l : list A) (n : Z) : option A :=
  match l with
  | nil => None
  | a :: tl =>
    if n =? 0 then Some a else Znth tl (n - 1)
  end.
\end{verbatim}
This {\tt Znth} function corresponds to the operation of accessing an
array, but its type reflects the fact that trying to access out of the
bounds of the array should return an error.  Modeling the behavior of
a program using this function is a way to express that we will explicitely
show that there are no out of bounds bugs in the program.

\item The function {\tt app} is already provided in the {\tt List} library.
  It takes two lists and returns the concatenation of these two lists.
  There is an infix notation that is also predefined for this function, so
  that {\tt app l1 l2} is also written {\tt l1 ++ l2}.  This notation is
  right associative, so that {\tt l1 ++ (l2 ++ l3)} means {\tt l1 ++ l2 ++ l3}.
\item The function {\tt rev} is already provided in the {\tt List} library.  It
takes as input a list \( a_1 {\tt ::} a_2 \cdots {\tt ::} a_n {\tt ::~nil}\) and
returns the list \(a_n {\tt ::} a_{n-1} \cdots {\tt ::} a_1 {\tt ::~nil}\).
\end{enumerate}

\subsection{Logical connectives}
There are four simple connectives {\em implies}, {\em and}, {\em or},
{\em not}.  These connectives are given with notations that make it possible
to write logical formulas in a short and readable way.
\begin{itemize}
\item Implication is noted {\tt ->}.
\item Conjunction ({\em and}) is noted {\tt \coqand{}}.
\item Disjunction ({\em or}) is noted {\tt \coqor{}}.
\item Negation is noted {\tt \coqnot{}}.
\item Equivalence is noted {\tt <->}.  In fact {\tt A <-> B} is a
  defined as {\tt A -> B \coqand{} B -> A}.
\end{itemize}

For instance, we can write the following proposition concerning the function
{\tt Znth}.
\begin{alltt}
Check ~(Znth (1 :: 2 :: nil) 1 = None) ->
   0 <= 1 < Zlength (1 :: 2 :: nil).
\textcolor{blue}{~(Znth (1 :: 2 :: nil) 0 = None) ->
       0 <= 1 < Zlength (1 :: 2 :: nil) : Prop}
\end{alltt}
It happens that this proposition is provable, as we shall see later.
\subsection{Quantifications}
We can also write formulas that give properties about a whole type.
There are two main connectives for this.  The first one, {\tt forall},
describes {\em universal quantification}.  In other words, this is
used to express that every element of a type satisfies some property.
The second one, {\tt exists}, describes {\em existential
  quantification}.  This is used to express that at least one element
of the type satisfies some property.  For instance, one can write the
following proposition.

\begin{verbatim}
Check forall {A : Type} l x, Znth l x -> 0 <= x < Zlength l.
\end{verbatim}
It happens that this proposition can be proved, as we shall see later.
Here, the example chosen as illustration is true and can be proved.
In a sense, this means that we can prove that the {\tt Znth} and the
{\tt Zlength} function are consistent with the explanations in plain
language.  However, we cannot write a test that guarantees this property,
because such a test would have to cover all possible lists with elements in
all possible types and with all possible lengths.

\subsection{Playing with logical formulas}
To progress further you should be able to write simple logical
formulas that carry the meaning of some of your knowledge about real
life, programs, or mathematical facts.

In general, if you write {\tt A -> B} and you mean this formula to be
true, it is not necessary that {\tt A} is always true: you only want
to express that {\tt B} is true in all cases where {\tt A} is true,
but {\tt A} may sometimes be false and in those cases, {\tt B} can
also be false.  On the other
hand, if you write {\tt A \coqand{} B} and you mean this formula to be
true, then {\tt A} should always be true.

Here is a sentence: {\em in a non-empty list of integers, there is
 always a number that is smaller than all the others}.

The first part of the sentence expresses that we want to discuss about
{\em any} list of integers that is non-empty.  We shall naturally have
a universal quantification over a type, but the types we know do not make
it possible to express that we want to look at non-empty lists.  Therefore,
we will proceed in two states: first we take all possible lists of integers,
and then we qualify the list we discuss using an implication.  We start
writing our formula in this way.
\begin{verbatim}
forall l : list Z, ~ (l = nil) -> ...
\end{verbatim}
We can now think about the formula we want to put in the dotted part of the
formula.  There sentence we want to translate says
{\em there is always a number}.  This part of the sentence is going to be
translated using the following.
\begin{verbatim}
exists x, ...
\end{verbatim}
Now, we need to express that this number satisfies two properties.  First,
we want to express that the chosen number is in the list, and second we want
to express that the chosen number is smaller than all the others.

To express that the number is in the list, we use the predefined predicate
{\tt In}, so we write {\tt In x l}.

To express that the number is smaller than any other element of the list,
we want to discuss about every element in the list, so we want again
to write a universal quantification, restricted to all elements of the
list.
\begin{verbatim}
forall y, In y l -> ...
\end{verbatim}
Then we want to say that {\tt x} is smaller than {\tt y}.  Here we have to
be careful if we want to state a formula that is actually provable.  The
natural language sentence says {\em that is smaller than all the others}.
Resolving pronouns in this sentence, {\em that} would be represented by
{\tt x} and all the others would be represented by {\tt y}.  But to express
that {\tt x} is smaller than {\tt y}, we should avoid writing {\tt x < y},
because {\tt x} actually be equal to {\tt y}, because {\tt y} can be taken
among all the elements of the list.  We should rather use {\tt x <= y}.

So to finish the translation of this natural language sentence into a
logical formula, we obtain the following text:
\begin{verbatim}
forall l : list Z, ~(l = nil) ->
  exists x, In x l /\ forall y, In y l -> x <= y
\end{verbatim}
There are quite a few common mistakes that one makes when writing such a
formula.  One of the first mistakes is to get the parentheses in the wrong
place.  For better readability, we tend to lower the number of parentheses
we use, but it may worth the effort to write too many parentheses at the
first try and check with the computer that the formula can be written
with parentheses.

The second common mistake that people make often is to use an {\tt \coqand}
operator in place of an {\tt ->} operator.  The rule of thumb is as follows:
as an immediate subterm of an existential quantification, it is more frequent
to have an {\tt \coqand} operator, and at an immediate subterm of a universal
quantification, it is more frequent to have an {\tt ->} operator.

If we consider the following formula:
\begin{verbatim}
forall l : list Z, ~(l = nil) ->
  exists x, In x l -> forall y, In y l -> x <= y
\end{verbatim}
It is easy to prove that formula by taking as witness any number that
{\em is not} in the list.  the implication
{\tt In x l -> forall y, In y l -> x <= y} is easy to prove, because
the left hand side of the implication is false.  So this mistaken formula
does not mean that we found an element that is smaller than all elements of
the list.

In the end, we know that a logical formula is right when we are certain
we can prove it and when we can use it in other proofs.

To prove the formula we used in this section, we need to exhibit a function
that computes the minimum element of a list, and then to show that this
element satisfies the required property.  With what you already know
about programming with list and testing numbers for comparison,
you can alsready define such a function.

\section{Performing simple proofs}
To perform a proof, we state the proposition we want to prove, then we
decompose the proposition into simpler propositions, until they can be
solved.  The commands used to decompose propositions are called
tactics.  To learn how to use the tactics, it is handy to classify
them according to the connectives and according to whether the
connectives appears in something we want to prove or in something we
already know.
\subsection{Stating a logical formula to prove}
It is better to show that in an example.
\begin{alltt}
Lemma ex1 : 2 = 3 -> 3 = 2.
Proof.
\end{alltt}
The keyword is {\tt Lemma}: we will use it every time we want to start
a new proof.  Then comes a unique name, which we choose, {\tt ex1}.
Then comes a colon ``{\tt :}''.  Then comes a logical formula (here an
implication between two equalities).  We finish the command with a
period.
The next line {\tt Proof.} is mostly useless, but we will keep the habit
of writing it as it can be used as a marker to make the proof script more
readable.
\subsection{Known facts and facts to prove}
At any time during a proof, the Coq system displays {\em goals}, which
describe what we have to prove.  Each goals has two parts, the first
part is a {\em context} of temporary known facts.  The second part is a
{\em conclusion}, a fact that needs to be proved.  A simple case of
proof is when the fact we want to prove is present among the known
facts.  In this case, it suffices to use the basic tactic {\tt
  assumption} to solve the goal.  If we do that, the Coq system
displays the next unsolved goal, or says that the proof is complete.

There are a few other easy cases that are recognized by the proof system, the
command to solve these easy cases is called {\tt easy}.  For beginners,
it is sometimes puzzling, because it often fails to prove facts that seem
obvious to us, while it may solve questions that require a little thinking
on our side (especially when the fact requires some computation).
\subsection{Finishing a proof}
When there are no more subgoals to solve, the system says that the proof is complete, but we still have an operation to do: we must instruct the system to record the completed proof in memory.  This is done by typing in the command {\tt Qed.}


\subsection{Handling connectives}
\subsubsection{implication}
When a goal's conclusion is an implication, we can make it simpler by applying the tactic
{\tt intros H}.  This produces a new goal with an extra element in the context, which corresponds to the proposition that was initially in the left hand side of the implication.
\begin{alltt}
\textcolor{blue}{  H : A
  ==================
  2 = 3 -> 3 = 2}

intros H'.
\textcolor{blue}{  H : A
  H' : 2 = 3
  ==================
  3 = 2}
\end{alltt}
The name used as argument to the {\tt intros} tactic is used to name the new fact in the context.

To use an hypothesis that contains an implication, we use the tactic {\tt apply}.  Here is an example.
\begin{alltt}
\textcolor{blue}{  H : A -> 2 = f 3
  ==================
  2 = f 3}

apply H.
\textcolor{blue}{  H : A -> 2 = f 3
 ==================
  A}
\end{alltt}
This works only if the left hand side of the hypothesis {\tt H} corresponds exactly with the conclusion.  The conclusion is replaced by the left-hand side of the arrow.  If there are several arrows in the hypothesis, then several goals are produced.  For instance, if the hypothesis had been
{\tt A -> B -> 2 = 3}, then we would have had two new goals, one with {\tt A} and the other with {\tt B}.

\subsubsection{Conjunction}
When a goal's conclusion is a conjunction, we can make it simpler by applying the tactic
{\tt split}.  This produces two new goals whose statements are the parts of the conjunction.

\begin{alltt}
\textcolor{blue}{  H1 : A -> B
  =======================
  C \coqand{} D}
split.
\textcolor{blue}{  H1 : A -> B
  =======================
   C

Subgoal 2 is:
   D}
\end{alltt}

When we want to use an hypothesis that is a conjunction, we often need to decompose this conjunction to extract its parts as new hypotheses.  This is done with the destruct command.
\begin{alltt}
\textcolor{blue}{  H : A \coqand{} B
  =====================
   A}
destruct H as [H1 H2].
\textcolor{blue}{  H1 : A
  H2 : B
  =====================
   A}
\end{alltt}
\subsubsection{Disjunction}
When a goal's conclusion is a disjunction, we can make it simpler by applying one of the tactics
{\tt left} and {\tt right}.  This produces a new goal with only the chosen part of the goal.

\begin{alltt}
\textcolor{blue}{  H1 : A -> B
  =======================
  B \coqor{} 2 = 3}
left.
\textcolor{blue}{  H1 : A -> B
  =======================
   B}
\end{alltt}
Of course, we have to be careful and choose the tactic that will really lead us to a new goal that is provable (in this example, choosing {\tt right} looks silly).

When we want to use an hypothesis that is a disjunction, we often need
to decompose this conjunction to extract its parts as new hypotheses.
But this produces two goals, because if we have to cover the two cases:
\begin{alltt}
\textcolor{blue}{  H : A \coqor{} B
  =====================
   B \coqor{} A}
destruct H as [H1 | H2].
\textcolor{blue}{  H1 : A
 =====================
   B \coqor{} A

Subgoal 2 is:
   B \coqor{} A}
\end{alltt}
The second goal contains an hypothesis named {\tt H2} (as stated in the {\tt destruct} tactic) with {\tt B} as the statement.
\subsubsection{Negation}
When a goal's conclusion is a negation, we can make it simpler by applying the tactic
{\tt intros H}.

\begin{alltt}
\textcolor{blue}{  H1 : A -> B
  =======================
   \coqnot{} A}
intros H.
\textcolor{blue}{  H1 : A -> B
  H : A
  =======================
   False}
\end{alltt}
When proving ``not A'', the idea is to show that assuming {\tt A} would lead to a contradiction.

When we want to use an hypothesis that is a negation, we apply a tactic called {\tt case H}.  This replaces the current conclusion by the negated formula.
\begin{alltt}
\textcolor{blue}{  H : \coqnot{} A
  =====================
   C}
case H.
\textcolor{blue}{  H : \coqnot{} A
 =====================
   A}
\end{alltt}
We should do this exactly when we know that we will be able to prove {\tt A} more easily that proving {\tt C}.
\subsection{Quantifiers}
\subsubsection{Universal quantification}
When trying to prove a universally quantified formula, we often use the
tactic {\tt intros x}, where {\tt x} is a name chosen to fix the value
on which we want to reason.  The idea is that if we want to prove a formula
for all members of a type, we should simply prove that this
formula holds for a single arbitrary one, which we choose to name {\tt x}.
Because {\tt x} is taken arbitrarily, everything we prove about it is
universal.

Here is an example:
\begin{alltt}
\textcolor{blue}{  ==================
  forall A:Prop, A -> A}
intros A.
\textcolor{blue}{  A : Prop
  ==================
   A -> A}
\end{alltt}
This proof can be finished by typing {\tt intros H.} and then {\tt assumption.}

We shall see in another lesson that some universally quantified formulas can be proved by more advanced means, like {\tt induction}.

If one wants to use an hypothesis that starts with a universal quantification,
one should most of the time use the tactic {\tt apply}.  Here is an example:
\begin{alltt}
\textcolor{blue}{  H : forall x: Z, P x -> Q x
  ================
   Q 3}
apply H.
\textcolor{blue}{  H : forall x: Z, P x -> Q x
  ================
   P 3}
\end{alltt}
Note that the Coq system found an instance of the universally
quantified formula that corresponds to {\tt Q 3}, then it applied the
same behavior as for implication.
\subsubsection{Existential quantification}
When trying to prove an existentially quantified formula, we have to
provide a candidate value that satisfies the required predicate.  The
tactic is called {\tt exists} (with the same spelling as the logical
connective).  Here is an example.
\begin{alltt}
\textcolor{blue}{  ======================
   exists x, 2 * x = 6}
exists 3.
\textcolor{blue}{  ======================
   2 * 3 = 6}
\end{alltt}
As a result, we simply have to prove that the provided value (here 3)
satisfies the required predicate.

When trying to use an hypothesis that starts with an existential
quantification, we actually want to decompose the information in this
quantification, so that we obtain a new context that really contains a
value satisfying the property of interest.  Here is an example:
\begin{alltt}
\textcolor{blue}{  H : exists x : Z, Znth l x = Some v.
   ======================
    C}
destruct H as [w Pw].
\textcolor{blue}{  w : Z
  Pw : Znth l w = Some v
  =======================
   C}
\end{alltt}
In the goal before the {\tt destruct} tactic, there is no integer that
satisfies the property.  In the goal after the tactic, there is an integer
called {\tt w} and we know that {\tt w} satisfies the property, so we
can use it for various purposes.
\subsection{Predicates}
\subsubsection{Equality}
When we want to prove an equality, the simplest approach is when the two members of the equality are equal (even modulo computation).  Here is an example.
\begin{alltt}
\textcolor{blue}{  =====================
  2 = 3 - 1}
reflexivity.
\textcolor{blue}{  No more subgoals.}
\end{alltt}
This tactic can also be used if the computation uses functions that
we have defined ourselves, like {\tt Znth}.

If we want to use an hypothesis that contains equalities, we can use the
{\tt rewrite} tactic.
\begin{alltt}
\textcolor{blue}{  H : 3 = 2
  ====================
   2 = 3}
rewrite H.
\textcolor{blue}{  H : 3 = 2
  ====================
   2 = 2}
\end{alltt}

The tactic {\tt rewrite} can also be used if the equality is wrapped inside a universal quantification.  In that case, it finds the first relevant instantiation of the universally quantified variable before performing the replacement.
\begin{alltt}
\textcolor{blue}{  H : forall x : Z, f (f x) = g (x + x)
  ====================
   g (2 + 2) = E}
rewrite <- H.
\textcolor{blue}{  H : forall x : Z, f (f x) = g (x + x)
  ====================
   f (f 2) = E}
\end{alltt}
Note also, that the {\tt <-} modifier makes it possible to use the equality in
a different direction.

\section{Un exemple de preuve}
\begin{verbatim}
Lemma distr_or_comm_l :
  forall a b c, a \/ (b /\ c) -> (a \/ b) /\ (a \/ c).
intros a b c h.
destruct h as [ha | hconj].
 split.
  left.
  exact ha.
 left.
 exact ha.
destruct hconj as [hb hc].
split.
 right.
 exact hb.
right.
exact hc.
Qed.
\end{verbatim}


\section{Exercises}
\begin{enumerate}
\item Write a predicate {\tt multiple} of type {\tt Z -> Z ->
    Prop}, so that
{\tt multiple a b} expresses that {\tt a} is a multiple of {\tt b} (in
other words, there exists a number {\tt k} such that {\tt a = k * b}).
\item Write a formula using integers that expresses that when
{\tt n} is a multiple of 2 then {\tt n * n} is also a multiple of 2.
\item Write a formula using integers that expresses that when a
  number {\tt n} is a multiple of some {\tt k}, then {\tt n * n} is a
  multiple of {\tt k} (you don't have to prove it yet).
\item define a predicate {\tt odd} of type {\tt Z -> Prop} that
  characterize odd numbers like 3, 5, 37.
\item Assuming there exists a type {\tt T} used to represent integers
  and a function {\tt T\_to\_Z}  of type {\tt T -> Z}, which maps any
  element of {\tt T} to the element of {\tt Z} that it represents,
  and assuming that {\tt tadd} is a function of type {\tt T -> T ->
    T}, how do you express that {\tt tadd} represents addition?
  Beware that several elements of {\tt T} may represent the same
  element of {\tt Z}.
\item Write the script that proves the following formula
\begin{alltt}
forall P Q : Z -> Prop,
forall x y : Z, (forall z, P z -> Q z) -> x = y -> P x ->
  P x \coqand{} Q y
\end{alltt}
\item Write the script that proves the following formula
\begin{alltt}
forall A B C : Prop, (A \coqand{} B) \coqor{} C -> A \coqor{} C
\end{alltt}
\item Write the script that proves the following formula
\begin{alltt}
forall P : Z -> Prop, (forall x, P x) ->
 exists y : Z, P y \coqand{} y = 0
\end{alltt}
\item Write the script that proves that when {\tt n} is a multiple of
  {\tt k}, then {\tt n * n} is also a multiple of {\tt k}.  You will need
  a theorem to reason about associativity of multiplication between integers.
  Use {\tt Search (\_ * \_ * \_).} to find such a theorem.

\item Write the script that proves that when {\tt n} is odd, then {\tt
    n * n} is also odd.  Again, use {\tt Search} to find relevant theorem.

\end{enumerate}
\section{More information}
Vous pouvez utiliser le livre \cite{coqart} (disponible en fran�ais sur internet) et le manuel de r�f�rence
\cite{coqmanual}.  Il existe aussi un tutoriel en fran�ais par un autre professeur \cite{surviecoq}.
Il y aussi des tutoriels en anglais
\cite{coqtutorial,coqhurry}.
\begin{thebibliography}{10}
\bibitem{coqart}
Y.~Bertot and P.~Cast{\'e}ran.
\newblock {\em Interactive Theorem Proving and Program Development, Coq'Art:the
  Calculus of Inductive Constructions}.
\newblock Springer-Verlag, 2004.
\bibitem{softwarefoundations}
B.~Pierce et al.
\newblock{\em Software Foundations}
\newblock{\url{http://www.cis.upenn.edu/~bcpierce/sf/}}
\bibitem{coqhurry}
Y.~Bertot
\newblock{\em Coq in a Hurry}
\newblock Archive ouverte ``cours en ligne'',  2008.
\newblock {\url{http://cel.archives-ouvertes.fr/inria-00001173}}
\bibitem{coqmanual}
The Coq development team.
\newblock {\em The Coq proof Assistant Reference Manual},
\newblock Ecole Polytechnique, INRIA, Universit� de Paris-Sud, 2004.
\newblock {\url{http://coq.inria.fr/doc/main.html}}
\bibitem{coqtutorial}
G.~Huet, G.~Kahn, C.~Paulin-Mohring,
\newblock {\em The Coq proof Assistant, A Tutorial},
\newblock Ecole Polytechnique, INRIA, Universit� de Paris-Sud, 2004.
\newblock {\url{http://coq.inria.fr/V8.1/tutorial.html}}
\bibitem{tutreccoq}
E.~Gim�nez, P.~Cast�ran,
\newblock{\em A Tutorial on Recursive Types in Coq},
\newblock INRIA, Universit� de Bordeaux, 2006.
\newblock{\url{http://www.labri.fr/Perso/~casteran/RecTutorial.pdf.gz}}
\bibitem{surviecoq}
A.~Miquel,
\newblock{\em Petit guide de survie en Coq},
\newblock{Universit� de Paris VII}.
\newblock{\url{http://www.pps.jussieu.fr/~miquel/enseignement/mpri/guide.html}}
\end{thebibliography}
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
