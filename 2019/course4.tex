\documentclass{article}
\usepackage{url}
\usepackage{alltt}
\usepackage{color}
\usepackage[latin1]{inputenc}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqnot}{\mbox{\~{}}}
\newcommand{\coqor}{{\tt\char'134/}}
\title{Verifying programs and proofs\\
part IV. Proofs about arithmetic programs}
\author{Yves Bertot}
\date{October 2019}
\begin{document}
\maketitle
\section{Motivating introduction}
Many proofs by induction just fall through without thinking, but some
functions are more tricky than simple traversal of data-structure:
they use accumulators, or they call themselves on other arguments than
the immediate subterms of the input.  For these function, proofs by
induction is still the main tool, but such proofs must be planned
carefully.

\section{Advanced proof by induction for numbers}
When proving statements by induction, it may be easier to prove
stronger statements.  This may seem paradoxal, because stronger
statements will be harder to prove.  However, in proofs by induction
the fact to be proved is repeated in the induction hypothesis, which
also becomes stronger.

\subsection{Multi-step induction}
The rule for defining a recursive function is that it can call itself on a
different argument than an immediate subterm, as long as this argument can be
traced back to the original argument through a descent in the structure.
However, the induction principle associated to a given function is still
usually restricted to only the immediate subterm.  General solution exist, but
for the moment we shall give a limited approach that shows how to overcome
this hurdle.  The idea is to prove a statement that covers the descent down to
last but one step.

Here is an example to illustrate this idea.  The first few lines of
the following proof are used to show that plain induction fails.
\begin{alltt}
Require Import Arith Psatz.

Fixpoint mod2 (n : nat) :=
  match n with S (S p) => mod2 p | a => a end.

Lemma mod2_lt_2 : forall n, mod2 n < 2.
Proof.
induction n; simpl; [lia | ].
\textcolor{blue}{  n : nat
  IHn : mod2 n < 2
  ============================
   match n with
   | 0 => S n
   | S p => mod2 p
   end < 2}
destruct n;[lia | ].
\textcolor{blue}{
  n : nat
  IHn : mod2 (S n) < 2
  ============================
   mod2 n < 2}
\end{alltt}
Invoking {\tt destruct} is natural because the goal's conclusion
contains a pattern-matching construct.  But the goal that we obtain
is not easy to prove: there is a mismatch between the statement
that concerns {\tt n} and the hypothesis that concerns {\tt S n}.  We
should abort this proof and start afresh, with a stronger statement.

\subsection{Fixed multi-step induction}
The recursive call of function {\tt mod2} is made on a {\tt p} such that
the successor of {\tt p} is not the initial argument, but the successor of
{\tt S p} is.  So, we are only missing one step.  To cope with the problem,
we prove a conjunction of the fact that is interesting to us and the similar
fact for {\tt S p}.  Here is the complete proof.

We use the tactic {\tt assert} to introduce the stronger statement.
\begin{alltt}
Lemma mod2_lt : forall n, mod2 n < 2.
intros n; assert (H : mod2 n < 2 \coqand{} mod2 (S n) < 2);
 [ | destruct H; assumption].
\textcolor{blue}{  n : nat
  ============================
   mod2 n < 2 \coqand{} mod2 (S n) < 2}
induction n;[simpl; lia | ].
\textcolor{blue}{  n : nat
  IHn : mod2 n < 2 \coqand{} mod2 (S n) < 2
  ============================
   mod2 (S n) < 2 \coqand{} mod2 (S (S n)) < 2}
\end{alltt}
The new goal is more complex: we now have to prove a conjunction
instead of an equality.  But the induction hypothesis is also a
conjunction, and the right-hand side of the hypothesis coincides with
the left hand-side of the goal's conclusion.  Thus, we can destruct
the hypothesis and solve the first part of the proof quickly.
\begin{alltt}
destruct IHn as [H1 H2]; split;[assumption | ].
\textcolor{blue}{  n : nat
  H1 : mod2 n < 2
  H2 : mod2 (S n) < 2
  ============================
   mod2 (S (S n)) < 2}
\end{alltt}
By computation, {\tt mod2 (S (S n))} is the same as {\tt mod2 n}.
Thus, this goal is easily solved using the {\tt assumption} tactic.
\subsection{course-of-value induction}
The example given in the previous section shows that we can have
induction hypothesis not only on the predecessor of a number, but on
the predecessor and the predecessor of the predecessor.  Sometimes, we
may want to have an induction hypothesis on all numbers smaller than a
number.

There exists a theorem that covers this kind of need, but it applies
in a more general setting.  The name of the theorem is {\tt
  well\_founded\_ind} and it applies to a variety of binary relations,
as long as they satisfy the property to be ``well founded''.  The
relation {\tt lt} satisfies this property.  Thus we can combine two
theorems together to obtain an induction principle that is stronger
than the basic one we have already used.
\begin{alltt}
Check well_founded_ind lt_wf.
\textcolor{blue}{well_founded_ind lt_wf
 : forall P : nat -> Prop,
   (forall x : nat, (forall y : nat, y < x -> P y) -> P x) ->
   forall a : nat, P a}
\end{alltt}
Let's read carefully the statement of this theorem.  It is universally
quantified over a predicate {\tt P}.  It has only one case to cover
(instead of two for a regular induction principle) but this case
contains induction hypotheses for all numbers less than the number for
which the property needs to be proved.  If we manage to prove this
single case, the predicate holds universally.

For the number 0, there is no other number that is less than 0, so
there is no number for which induction hypotheses hold.  So this is
similar to the base case of the conventional induction principle.

This is illustrated in an example reasoning on an implementation of a
division function on natural numbers.  This function returns a pair
containing the quotient and the remainder of the division.  Note that
this function is tuned to handle divisions by 0 gracefully.  When
dividing by 0, the quotient is set to 0 and the remainder is the
number being divided.
\begin{alltt}
Fixpoint div (n m : nat) :=
 match m, n with
   S m', S n' => 
   if leb m n then
      let (q, r) := div (n' - m') m in (S q, r)
    else
     (0, n)
 | _, _ => (0, n)
 end.
\end{alltt}
When this function computes it has recursive calls, where the second
argument never decreases (the initial value is {\tt m} the value
of the second argument in the recursive call is also {\tt m}).  The
first argument decreases by a variable amount.  For instance, if {\tt
  m} is 1, then {\tt m'} is 0, and if we compute {\tt div 7 1}, then the
recursive calls are {\tt div 6 1}, {\tt div 5 1}, etc.  On the other
hand, if {\tt m} is {\tt 3}, then {\tt m'} is {\tt 2}, and if we
compute {\tt div 7 3}, then the recursive calls are {\tt div 4 3} and
{\tt div 1 3}.

We can now prove a simple equality about this division function.
\begin{alltt}
Lemma div_eq :
   forall n m, n = m * fst (div n m)  + snd (div n m).
intros n; induction n  as [n IHn] using (well_founded_ind lt_wf).
\textcolor{blue}{  n : nat
  IHn : forall y : nat,
        y < n -> forall m : nat,
                    y = m * fst (div y m) + snd (div y m)
  ============================
   forall m : nat, n = m * fst (div n m) + snd (div n m)}
\end{alltt}
As we see here, the induction hypotheses states that every {\tt y}
smaller than {\tt n} satisfies the property that we want to prove for
{\tt n}.

The rest of this proof is left as an exercise.  Another exercise
consists in proving that the remainder of the division is smaller than
the divisor, when this divisor is not 0.
\section{Strengthening a statement by universal quantification}
For lists, the technique of strengthening induction also applies.  Sometimes,
the induction is actually disguised as an induction on natural numbers,
by using the size of the list for example.  Some other times, functions
have several arguments and it is necessary to have universal quantification
on some of the arguments instead of fixed values.

To
illustrate this, let's look at a proof concerning reversed lists.

There are two ways to reverse lists, but only one is efficient.
\begin{alltt}
Require Import List.

Fixpoint slow_rev_nat (l : list nat) : list nat :=
  match l with
    nil => nil
  | a::l' => slow_rev_nat l' ++ (a::nil)
  end.

Fixpoint pre_rev_nat (l l' : list nat) : list nat :=
  match l with
    nil => l'
  | a :: l => pre_rev_nat l (a::l')
  end.

Definition rev_nat (l : list nat) : list nat :=
   pre_rev_nat l nil.
\end{alltt}
In the definition of {\tt slow\_rev\_nat}, we use a function {\tt

  app}, with an infix notation {\tt ++} to concatenate two lists.
It is easily to be convinced that {\tt slow\_rev\_nat} reverses a
list: when working on a list with a first element {\tt a}, this
element ends up in the end and the rest of the list is also reversed.
So we can use it as a reference implementation, but it is slow, because
concatenating takes a time proportional to the length of the first
list being treated.  In the end, the complexity of this function is
quadratic.

The other function is more efficient: reversal takes place in linear
time.  We can see that by testing the functions on list of increasing
length.  So it is useful to have both functions available.  The first
one can be used in specifications, the second one in implementations.

If we want to express the correctness of the efficient function, we
can just write the following statement.
\begin{alltt}
Lemma rev_nat_correct : forall l, rev_nat l = slow_rev_nat l.
\end{alltt}
If we start our proof directly by an induction we discover that the
statement loses its beautiful shape and the proof gets stuck in the
recursive case.
\begin{alltt}
induction l as [ | a l' IHl'].
\textcolor{blue}{
  ======================
   rev_nat nil = slow_rev_nat nil}
reflexivity.
simpl.
\textcolor{blue}{
  IHl' : rev_nat l' = slow_rev_nat l'
  ======================
   rev_nat (a::l') = slow_rev_nat l'::(a::nil)}
\end{alltt}
At this point, there is no simple way to show a correspondance between
{\tt rev\_nat a::l'} and {\tt rev\_nat l'}; mainly because the latter
is not a subterm of the former.  This proof gets stuck.

To repair this proof, we can rely on a stronger statement, manipulating
the function {\tt pre\_rev\_nat} and quantifying over both its
arguments.
\begin{alltt}

Lemma rev_nat_correct : forall l, rev_nat l = slow_rev_nat l.
assert (forall l l', pre_rev_nat l l' = slow_rev_nat l ++ l').
 induction l.
  intros; reflexivity.
 intros l'; simpl.
  rewrite <- app_assoc.
 simpl; apply IHl.
intros l; unfold rev_nat; rewrite H, <- app_nil_end; reflexivity.
Qed.
\end{alltt}
\section{Exercises}
\begin{enumerate}
\item Here is a definition of division by 2 in natural numbers:
\begin{verbatim}
Fixpoint div2 (n : nat) :=
 match n with S (S p) => S (div2 p) | _ => 0 end.
\end{verbatim}
Using the definition of {\tt mod2} from these course notes, prove the
following lemma:
\begin{verbatim}
Lemma mod2_div2_eq n : n = mod2 n + 2 * div2 n.
\end{verbatim}
\item Prove that {\tt forall n, div2 (2 * n) = n}.
\item Prove that a number cannot be at the same time odd and even.
\item Here is a definition of sorting for lists of natural numbers.
\begin{verbatim}
Fixpoint insert (a : nat) (l : list nat) :=
  match l with
    nil => a::nil
  | b::tl => if Nat.leb a b then a::b::tl else b::insert a tl
  end.

Fixpoint sort (l : list nat) :=
  match l with nil => nil | a::tl => insert a (sort tl) end.
\end{verbatim}
To express the correctness of the {\tt sort} function, we construct
to extra testing functions.
\begin{verbatim}
Fixpoint count (x : nat) (l : list nat) :=
  match l with
    nil => 0
  | y::tl =>
    if Nat.eqb x y then 1 + count x tl else count x tl
  end.

Fixpoint sorted (l : list nat) :=
  match l with
    a :: (b :: tl) as l' =>
    if Nat.leb a b then sorted l' else false
  | _ => true
  end.
\end{verbatim}

Correctness is then expressed with the following statements.  Prove them.
\begin{verbatim}
Lemma sort_perm : forall x l, count x l = count x (sort l).

Lemma sort_sorted : forall l, sorted (sort l) = true.
\end{verbatim}
\end{enumerate}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
