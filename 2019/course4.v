Require Import Arith Psatz.

Fixpoint mod2 (n : nat) :=
  match n with S (S p) => mod2 p | a => a end.

Lemma mod2_lt_2 : forall n, mod2 n < 2.
Proof.
induction n; simpl; [lia | ].
destruct n;[lia | ].
Abort.

Lemma mod2_lt : forall n, mod2 n < 2.
intros n; assert (H : mod2 n < 2 /\ mod2 (S n) < 2);
 [ | destruct H; assumption].
induction n;[simpl; lia | ].
destruct IHn as [H1 H2]; split;[assumption | assumption].
Qed.

Check well_founded_ind lt_wf.

Fixpoint div (n m : nat) :=
 match m, n with
   S m', S n' => 
   if leb m n then
      let (q, r) := div (n' - m') m in (S q, r)
    else
     (0, n)
 | _, _ => (0, n)
 end.

Lemma div_eq :
   forall n m, n = m * fst (div n m)  + snd (div n m).
intros n; induction n  as [n IHn] using (well_founded_ind lt_wf).
intros [ | m]; destruct n as [ | n]; simpl; auto.
  now rewrite Nat.mul_0_r, Nat.add_0_r.
destruct (m <=? n) eqn:cmp.
assert (mlen : m <= n) by now rewrite <- Nat.leb_le.
destruct (div (n - m) (S m)) as [q r] eqn: rec_call; simpl.
assert (IHn' : (n - m) = (S m) * fst (div (n - m) (S m)) +
               snd (div (n - m) (S m))).
  apply IHn; lia.
rewrite rec_call in IHn'; simpl in IHn'.
  lia.
now simpl; rewrite Nat.mul_0_r, Nat.add_0_l.
Qed.

Require Import List.

Fixpoint slow_rev_nat (l : list nat) : list nat :=
  match l with
    nil => nil
  | a::l' => slow_rev_nat l' ++ (a::nil)
  end.

Fixpoint pre_rev_nat (l l' : list nat) : list nat :=
  match l with
    nil => l'
  | a :: l => pre_rev_nat l (a::l')
  end.

Definition rev_nat (l : list nat) : list nat :=
   pre_rev_nat l nil.

Lemma rev_nat_correct : forall l, rev_nat l = slow_rev_nat l.
induction l as [ | a l' IHl'].
reflexivity.
simpl.
Abort.

Lemma rev_nat_correct : forall l, rev_nat l = slow_rev_nat l.
assert (forall l l', pre_rev_nat l l' = slow_rev_nat l ++ l').
 induction l.
  intros; reflexivity.
 intros l'; simpl.
  rewrite <- app_assoc.
 simpl; apply IHl.
intros l; unfold rev_nat; rewrite H, <- app_nil_end; reflexivity.
Qed.

Fixpoint div2 (n : nat) :=
 match n with S (S p) => S (div2 p) | _ => 0 end.

Lemma mod2_div2_eq n : n = mod2 n + 2 * div2 n.
Proof.
enough (n = mod2 n + 2 * div2 n /\ (S n) = mod2 (S n) + 2 * div2 (S n))
  by tauto.
induction n as [ | n Ih]; simpl; split; try tauto.
lia.
Qed.

Lemma div2_mul2 n : div2 (2 * n) = n.
Proof.
induction n as [ | n Ih].
  easy.
replace (div2 (2 * S n)) with (S (div2 (2 * n))); cycle 1.
  replace (2 * S n) with (S (S (2 * n))) by ring.
  easy.
now rewrite Ih.
Qed.

Definition odd n := mod2 n = 1.
Definition even n := mod2 n = 0.

Lemma not_odd_and_even n : ~ (odd n /\ even n).
Proof.
unfold odd, even; intros [odq evq]; rewrite odq in evq; discriminate.
Qed.

Fixpoint insert (a : nat) (l : list nat) :=
  match l with
    nil => a::nil
  | b::tl => if Nat.leb a b then a::b::tl else b::insert a tl
  end.

Fixpoint sort (l : list nat) :=
  match l with nil => nil | a::tl => insert a (sort tl) end.

Fixpoint count (x : nat) (l : list nat) :=
  match l with
    nil => 0
  | y::tl =>
    if Nat.eqb x y then 1 + count x tl else count x tl
  end.

Fixpoint sorted (l : list nat) :=
  match l with
    a :: (b :: tl) as l' =>
    if Nat.leb a b then sorted l' else false
  | _ => true
  end.

Compute sorted (3 :: 50 :: 4 ::  8 :: 9 :: 10 :: nil).

Lemma sort_perm : forall x l, count x l = count x (sort l).
Proof.
assert (insert_perm : forall x a l, count x (insert a l) = count x (a :: l)).
  intros x a; induction l as [ | b l Ih].
    easy.
  simpl.
  case (a <=? b) eqn:cmp.
    case (x =? a) eqn: testa.
      case (x =? b) eqn: testb.
        rewrite Nat.eqb_eq in testa; rewrite Nat.eqb_eq in testb.
        rewrite <- testb, testa; simpl.
        now rewrite Nat.eqb_refl.
     rewrite Nat.eqb_eq in testa.
     now rewrite <- testa; simpl; rewrite Nat.eqb_refl, testb.
   case (x =?b) eqn: testb.
     rewrite Nat.eqb_eq in testb; rewrite <- testb; simpl.
     now rewrite testa, Nat.eqb_refl.
   now simpl; rewrite testa, testb.
case (x =? a) eqn: testa.
  case (x =? b) eqn: testb.
    rewrite Nat.eqb_eq in testa; rewrite Nat.eqb_eq in testb.
    rewrite <- testb, testa; simpl.
    rewrite Nat.eqb_refl.
    now rewrite testa in Ih; rewrite Ih; simpl; rewrite Nat.eqb_refl.
  rewrite Nat.eqb_eq in testa.
  rewrite <- testa; simpl; rewrite testb.
  now rewrite testa in Ih |- *; rewrite Ih; simpl; rewrite Nat.eqb_refl.
  case (x =?b) eqn: testb.
   rewrite Nat.eqb_eq in testb; rewrite <- testb; simpl.
     now rewrite Nat.eqb_refl, Ih; simpl; rewrite testa.
   now simpl; rewrite testb, Ih; simpl; rewrite testa.
intros x l; induction l as [ | a l Ih].
  easy.
simpl.
now destruct (x =? a) eqn: testa; rewrite insert_perm; simpl; rewrite testa, Ih.
Qed.


Lemma insert_head a l  def : 
  (l <> nil -> hd def (insert a l) = a \/ hd def (insert a l) = hd def l).
Proof.
intros lnnil.
destruct l as [ | b l]; auto.
now simpl; destruct (a <=? b); auto.
Qed.

Lemma insert_sorted a l : sorted l = true -> sorted (insert a l) = true.
Proof.
induction l as [ | b l Ih]; auto.
simpl.
destruct (a <=? b) eqn: cmp.
  destruct l as [ | c l].
     now (simpl; rewrite cmp).
  destruct (b <=? c) eqn: cmp2.
    change (sorted (a :: b :: c :: l)) with
    (if a <=? b then if b <=? c then sorted (c :: l) else false else false).
    now rewrite cmp, cmp2.
  discriminate.
destruct l as [ | c l].
  simpl.
  rewrite Nat.leb_nle in cmp.
  assert (cmp' : b <= a) by lia.
  rewrite <- Nat.leb_le in cmp'.
  now rewrite cmp'.
destruct (b <=? c) eqn: cmp2; try easy.
destruct (insert a (c :: l)) as [ | d l'] eqn:ins'.
  simpl in ins'; destruct l; easy.
intros rec; apply Ih in rec.
assert (cmp3 : b <= d).
  rewrite Nat.leb_le in cmp2; rewrite Nat.leb_nle in cmp.
  assert (nnil : c :: l <> nil) by discriminate.
  assert (ihead := insert_head a (c :: l) a nnil); rewrite ins' in ihead.
  simpl in ihead.
  lia.
replace (sorted (b :: d :: l')) with 
  (if b <=? d then sorted (d :: l') else false) by easy.
now rewrite <- Nat.leb_le in cmp3; rewrite cmp3.
Qed.

Lemma sort_sorted : forall l, sorted (sort l) = true.
Proof. 
intros l; induction l as [ | a l Ih].
  easy.
now simpl; apply insert_sorted.
Qed.
