\documentclass{article}
\usepackage{url}
\usepackage{alltt}
\usepackage{color}
\usepackage[latin1]{inputenc}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqnot}{\mbox{\~{}}}
\newcommand{\coqor}{{\tt\char'134/}}
\title{Verifying programs and proofs\\
part III. prove program properties}
\author{Yves Bertot}
\date{October 2019}
\begin{document}
\maketitle
\section{Motivating introduction}
To prove that programs do what is intended, we need to cover all possible cases in their execution.  Most of the time, the programs take their input in types with an infinite numbers of elements.  It is thus impossible to check all possible inputs one by one.  Instead, we reason on subsets of the types that correspond to different behaviors of the considered programs.  This is usually done by following the structure of the program.  Thus we reason logically on the various cases that may arise during the execution of programs.

When functions are recursive, this approach relies on a complex logical tool, called {\em proof by induction}.  In this lecture, we want to understand the various aspects of reasoning about program behavior, including the idea of proofs by recursion.  We shall restrict this study to programs that compute on numbers and lists.

There are many ways to describe what is the expected behavior of a program.  In this course, we will study only a simple approach, where the expected behavior is described with the help of secondary
programs that are used either to produce specific inputs or to perform tests on on the output of a program.

For instance, if we wrote a function {\tt evenb} that computes a boolean value that is true whenever the input is an even number (a multiple of 2), we want to write a function that computes all even numbers.
\begin{alltt}
Definition mult2 (n : nat) := 2 * n.
\end{alltt}
Then we just want to prove that {\tt evenb} returns the correct value for every result of {\tt mult2}:
\begin{alltt}
Lemma evenb_complete :
  forall n : nat, evenb (mult2 n) = true.
\end{alltt}
This is not enough, because the trivial boolean predicate that always
returns true would also satisfy this lemma.  
We may also want to make sure that the function {\tt evenb} accepts only numbers that should be accepted.  One way to express this is with the following statement:
\begin{alltt}
Lemma evenb_sound :
  forall n : nat, evenb n -> exists y : nat, n = mult2 y.
\end{alltt}
Again, lemma {\tt evenb\_sound} is not enough, because the trivial
boolean predicate that always returns {\tt false} would also satisfy
this lemma.

In the end, every lemma that we write is like a symbolic test that we
run on the input program, but the proof shows that this test is
satisfied for all possible inputs instead of a random sample.  In
this sense, the coverage brought by formal proofs is more complete
than the coverage brought by random tests and people like to say that
we provide 100\% correctness, but we should keep in mind that the
lemmas may describe only partially the intended behavior of the
program (like the predicate {\tt
  evenb\_complete} and {\tt evenb\_sound} taken separately) and that the way we consider the inputs (like the function
{\tt mult2}) may also be faulty.

\section{Reasoning on pattern-matching constructs}
A pattern matching construct describes several possible cases of
execution.  When proving that a program is correct, we need to cover
all possibilities.  There are commands to decompose the problem and to
observe separately each of the cases.  Then, it may occur that some
cases are inconsistent, because they exhibit assumptions like {\tt 0 =
  1} or {\tt true = false}.  In some other cases, one may have
equalities of the form {\tt a::l = b::l'}, from which one should be
able to deduce at least {\tt a = b} and {\tt l = l'}.  We shall see
different approaches for this.
\subsection{case, destruct, and case\_eq}
Let us consider the following goal:
\begin{alltt}
  x : nat 
  ======================
   match x with 0 => true | S p => negb (even p) end = false
   -> x <> 0
\end{alltt}
The conclusion of this goal contains a pattern matching construct.  We
don't know the value of {\tt x}, but we know that {\tt x} is a natural
number, and by consequences {\tt x} may follow one of 2 cases: {\tt x}
is {\tt 0} or {\tt x} is {\tt S y} for some {\tt y}.  When {\tt x} is
{\tt 0}, the whole pattern-matching construct will compute to {\tt
  true}, so the goal's conclusion will become
\begin{alltt}
   true = false -> 0 <> 0
\end{alltt}
In the other case, we know that the pattern-matching construct will be
reduced to {\tt negb (even y)}, so the goal's conclusion will become
\begin{alltt}
  negb(even p) -> S y <> 0
\end{alltt}

There are three tactics that express this decomposition in two cases, with slightly different behaviors.  First, let's observe the behavior of the {\tt destruct} tactic.  It produces as many goals as the number of cases in the argument's type.  Here we use {\tt destruct x} and {\tt x} has type {\tt nat}.  Because the type {\tt nat} has two constructors, there are two cases: either {\tt x} is 0 or {\tt x} is {\tt S n} for some other natural number {\tt n}.

\begin{alltt}
destruct x.
\textcolor{blue}{2 subgoals
  =====================
   true = false -> 0 <> 0

Subgoal 2 is
   negb(even n) -> S n <> 0}
\end{alltt}
So the tactic simply produced two instances of the goal where {\tt x} is replaced with cases taken from the type.  In the first goal, all occurrences of {\tt x} are replaced by {\tt 0}, then the pattern matching construct is computed to take this new information into account.

In this example, the two goals can be proved because they mention equalities that cannot hold.  This is taken care of by the tactic described in the next section.

The tactic {\tt case} performs approximately the same operations as the tactic {\tt destruct}, except that its effect only applies in the conclusion of the goal and the data that is analyzed is not removed from the context.  So if we restart the example, we have the following behavior:
\begin{alltt}
\textcolor{blue}{  x : nat 
  ======================
   match x with 0 => true | S p => negb (even p) end = false
   -> x <> 0}
case x.
\textcolor{blue}{2 subgoals
  x : nat
  =====================
   true = false -> 0 <> 0

Subgoal 2 is
   forall n : nat, negb(even n) -> S n <> 0}
\end{alltt}
However, the variable {\tt x} that remains in the context is now completely disconnected from the values that replace it in the two goal conclusions.  That this variable is disconnected sometimes poses a problem.  Let us show an example where this is a problem.  Let's take again the same example, but assume that we perform a stronger call to {\tt intros} before calling {\tt case}.
\begin{alltt}
\textcolor{blue}{  x : nat 
  ======================
   match x with 0 => true | S p => negb (even p) end = false
   -> x <> 0}
intros H.
\textcolor{blue}{  x : nat
  H : match x with 0 => true | S p => negb (even p) end = false
  ======================
   x <> 0}
case x.
\textcolor{blue}{ x : nat
  H : match x with 0 => true | S p => negb (even p) end = false
  ======================
   0 <> 0

Subgoal 2 is
   forall n : nat, S n <> 0}
\end{alltt}
The first goal cannot be proved, because the instance of {\tt x} in the hypothesis {\tt H} has not been replaced by 0.

This problem is solved by another tactic named {\tt case\_eq}.  This tactic adds in the goals equalities that keeps a connection between {\tt x} and the values that replace it.
\begin{alltt}
\textcolor{blue}{  x : nat 
  H : match x with 0 => true | S p => negb (even p) end = false
 ======================
  x <> 0}
case_eq x.
\textcolor{blue}{2 subgoals
  x : nat
  H : match x with 0 => true | S p => negb (even p) end = false
  =====================
   x = 0 -> 0 <> 0

Subgoal 2 is
   forall n : nat, x = S n -> S n <> 0}1
\end{alltt}
This time, the occurrences of {\tt x} still remain unchanged in the hypothesis {\tt H}, but we now have
an equality in the goal that can be introduced and used to modify the hypothesis, with the help of the {\tt rewrite} tactic.
\subsection{the {\tt induction} tactic}
Reasoning by cases is not adapted for recursive functions, because we
often need hypotheses on the values returned by recursive calls.
These hypotheses often have the same form as the statement that one
attempts to prove.  This follows the a well-known pattern seen in
proofs about natural number, known as proof by induction.

\begin{description}
\item[induction on natural numbers] If a predicate on natural numbers {\tt P} is such that {\tt P 0} holds and for
every {\tt n} one can deduce {\tt P (S n)} from {\tt P n}, then this predicate holds for every natural number.
\item[induction on lists] If a predicate on lists of natural numbers {\tt P} is such that {\tt P nil}
holds and for every list {\tt l} and every natural number {\tt a}, if
{\tt P l} holds then we can deduce {\tt P (a::l)}, then this predicate
holds for every list of natural numbers.  This can be generalized to
list of any type of elements.
\end{description}
When performing a proof by induction on natural numbers, we also have two cases to study, the first case for the situation where the value is {\tt 0} and the second for the situation where the value has the form {\tt S n} for some {\tt n}.  In this respect, the induction tactic is very close to the {\tt destruct} tactic.  However, when considering the second case, we have more information: we can use an {\em induction hypothesis} stating that {\tt n} already satisfies the expected predicate.  Let's observe such a proof by induction concerning the addition of a number with 0.  We first observe the definition of addition:
\begin{alltt}
Locate "_ + _".
\textcolor{blue}{Notation            Scope     
"x + y" := sum x y   : type_scope
                      
"n + m" := plus n m  : nat_scope
                      (default interpretation)}
Print plus.
\textcolor{blue}{fix plus (n m : nat) {struct n} : nat :=
  match n with
  | 0 => m
  | S p => S (plus p m)
  end
     : nat -> nat -> nat}
\end{alltt}
We see that addition is given as a recursive function where the first
argument decreases at each recursive call.  By definition {\tt 0 + n}
computes to {\tt n} in a single step; on the other hand, computing
{\tt n + 0} does not do anything directly, but we can prove that it computes to {\tt n}.
Here is the proof in Coq:
\begin{alltt}
Lemma example_induction_plus : forall n, n + 0 = n.
induction n.
\textcolor{blue}{2 subgoals
  
  ============================
   0 + 0 = 0

subgoal 2 is:
 S n + 0 = S n}
\end{alltt}
As expected, we have two cases where {\tt n} is replaced either by {\tt 0} or by {\tt S n}.  For the first case, immediate computation yields the result.
\begin{alltt}
reflexivity.
\textcolor{blue}{1 subgoal
  
  n : nat
  IHn : n + 0 = n
  ============================
   S n + 0 = S n}
\end{alltt}
In this goal, the context contains the hypothesis {\tt IHn}, which states exactly that the property we want to prove already holds for {\tt n}.  So the induction principle is being used, and the predicate {\tt P} is instantiated with the function
\begin{alltt}
fun x => x + 0 = x
\end{alltt}
\subsection{the {\tt discriminate} tactic}
The datatypes of boolean values, natural numbers, and lists are all described in the Coq system as {\em inductive types}, where the data may each time correspond to two patterns.  We can see this by calling the command {\tt Print}.
\begin{alltt}
Print bool.
\textcolor{blue}{Inductive bool : Set :=  true : bool | false : bool}

Print nat.
\textcolor{blue}{Inductive nat : Set :=  O : nat | S : nat -> nat}

Print list.
\textcolor{blue}{Inductive list (A : Type) : Type :=
    nil : list A | cons : A -> list A -> list A}
\end{alltt}
The description of natural numbers means that numbers are either of the form {\tt O} or of the form {\tt S n}.  Moreover, it also means that the number {\tt 0} is not of the form {\tt S n}\footnote{This is also simply a consequence from the fact that we can define expressions by pattern-matching on natural numbers!}.  As a result, any goal whose conclusion has the form {\tt 0 <> S n} should be easily provable.  The Coq system provides a specific tactic for that, called {\tt discriminate}.  This tactic also takes care of cases where a goal has an arbitrary conclusion but one of its hypotheses is an hypothesis of the form \hbox{\tt O = S n}.

Continuing the example given in the previous section, we had two goals that we repeat again here:

\begin{alltt}
\textcolor{blue}{2 subgoals
  =====================
   true = false -> 0 <> 0

Subgoal 2 is
   negb(even n) -> S n <> 0}
\end{alltt}
For the first goal, we use the {\tt intros} tactic and we get a new hypothesis:
\begin{alltt}
intros Htf.
\textcolor{blue}{  ...
  Htf : true = false
  =====================
   0 <> 0}
\end{alltt}
In this goal, the conclusion would be unprovable in an empty context, but the hypothesis {\tt Htf} assumes an equality between two values that are different by definition.  This goal can be solved using the {\tt discriminate} tactic or more precisely the {\tt discriminate Htf}.

The second goal has the form
\begin{alltt}
  negb(even n) = true -> S n <> 0
\end{alltt}
Here the ultimate conclusion is the negation of an equality between two cases that are forced to be different.  So it falls in the same area of reasoning.  Here, the {\tt discriminate} tactics also solves the problem.
\subsection{the {\tt injection} tactic}
We often have to express that the constructors of types like {\tt nat} and {\tt list} are {\em injective}.  In other words, if they give equal outputs for two sets of inputs, then the inputs must be pairwise equal.  for lists this is easily expressed with the following example:

\begin{alltt}
Lemma example_injection_list :
  forall (a b : nat) (l1 l2 : list nat),  a::l1 = b::l2 ->
    a = b \coqand{} l1 = l2.
intros a b l1 l2 Hq.
\textcolor{blue}{  ...
  Hq : a::l1 = b::l2
  =========================
   a = b \coqand{} l1 = l2}
\end{alltt}
In this goal, the hypothesis {\tt Hq} describes an equality between two composed lists.  The conclusion expresses that the list components correspond to each other.  To go from {\tt Hq} to the conclusion, we call the tactic {\tt injection} with the name of the hypothesis.
\begin{alltt}
injection Hq.
\textcolor{blue}{  ...
  =========================
   l1 = l2 -> a = b -> a = b \coqand{} l1 = l2}
intros ql qa; rewrite ql qa; split; reflexivity.
Qed.
\end{alltt}
In the generated goal, two new implications are created, with the
equalities between components appearing as left-hand sides of these
implications.  The last two lines of the example show how to use these hypotheses.

Similarly, if we know two equal numbers that respect the {\tt S} pattern, we can
deduce that the sub-components are equal.
\begin{alltt}
Lemma example_injection_nat :
  forall (a b : nat),  Sa = S b -> a = b.
intros a b Hq.
\textcolor{blue}{  ...
  Hq : S a = S b
  =========================
   a = b}
injection Hq.
\textcolor{blue}{  ...
  =========================
   a = b -> a = b}
intros q1; exact q1.
Qed.
\end{alltt}

\section{Manipulating function computation}
In goals and logical statements, the Coq system manipulates functions without executing them.  We sometimes need to force at least a few steps of computation.

\subsection{The {\tt unfold} tactic}
  The first approach is to simply require that the system expands the definition.  The word used in Coq tactics is {\tt unfold}.

\begin{alltt}
Definition add3 (n : nat) := n + 3.

Lemma example_add3 : forall n, add3 n = 3 + n.
intros n.
\textcolor{blue}{  ...
  =====================
   add3 n = 3 + n.}
\end{alltt}
At this point, we would like to replace {\tt add3 n} with the expression it computes.  We use the {\tt unfold} tactic.

\begin{alltt}
unfold add3.
\textcolor{blue}{  ======================
    n + 3 = 3 + n}
\end{alltt}

\subsection{The {\tt simpl} tactic}
When dealing with a recursive function, the {\tt unfold} tactic often
makes goals unreadable, because it expands the value of the recursive function into something that repeats the text of the recursive function several times.  To avoid this, there is a tactic that specifically tuned to handle recursive function.  This tactic is called {\tt simpl}.

The example we have already seen about reasoning on the addition function provides an illustration for this.  Let start again with this proof.
\begin{alltt}
Lemma example_induction_plus : forall n, n + 0 = n.
induction n.
reflexivity.
\textcolor{blue}{1 subgoal
  
  n : nat
  IHn : n + 0 = n
  ============================
   S n + 0 = S n}
\end{alltt}
Here, we can request that Coq performs a little computation with {\tt S n + 0}.  We simply need to call the {\tt simpl} tactic:
\begin{alltt}
simpl.
\textcolor{blue}{  ...
  IHn : n + 0 = n
  ============================
   S (n + 0) = S n}
\end{alltt}
The left hand side of the equality in this goal's conclusion is an occurrence of the left hand side of the hypothesis {\tt H}.  We can rewrite and conclude the proof.
\begin{alltt}
rewrite IHn; reflexivity.
Qed.
\end{alltt}
\subsection{Manual computation: the {\tt change} tactic}
Sometimes the {\tt simpl} tactic performs too much computation.  In this case, it is a good idea to state explicitely the result that we want to see after computation, as long as this result really corresponds to a computation.  Here is an example.
\begin{alltt}
Lemma example_change_plus :
  forall n m p, (1 + n) * m = p -> (1 + (1 + n)) * m = m + p.
intros n m p H.
\textcolor{blue}{1 subgoal
  
 H : (1 + n) * m = p
  ============================
   (1 + (1 + n)) * m = m + p}
change ((1 + (1 + n)) * m) with (m + (1 + n) * m).
\textcolor{blue}{  ...
  ============================
   m + (1 + n) * m = m + p}
rewrite H; reflexivity.
Qed.
\end{alltt}
\subsection{Manual computation with the {\tt replace} tactic}
The tactic {\tt change} performs replacements only if the two expressions are the same modulo computation.  Sometimes, we want to relax the condition and perform replacement as soon as we can prove the equality between two expressions.  For this we use the {\tt replace} tactic.  This tactic produces a second goal, because the equality between the two expressions needs to be proved.
\subsection{Generating one-step recusion lemma}
Using the {\tt change} tactic gives the user complete control on what
is executed, but it is very cumbersome to use, as it requires that one
writes a lot of text.  One efficient approach is to provide an
unfolding lemma for the function that one produces.  This can easily
be done by copying the body of the function in an equality and
encapsulating it the universal quantifications that fit.  The proof
can usually be done by a simple case analysis on the first argument of
the function to be analysed. Here is an
example for a function of multiplication by 3:
\begin{verbatim}
Fixpoint mult3 (n : nat) : nat :=
match n with 0 => 0 | S p => 3 + mult3 p) end.

Lemma mult3_step (n : nat) :
  mult3 n =
  match n with 0 => 0 | S p => 3 + mult3 p) end.
Proof.
intros n; case n; reflexivity.
Qed.
\end{verbatim}

\section{Exercises}
\begin{enumerate}
\item Define a function {\tt lo} that takes a natural number {\tt n} as input and returns
 the list containing the first {\tt n} odd natural numbers.  For instance {\tt lo 3 = 5::3::1}.
\item Prove that {\tt length (lo n) = n}.
\item Define a function {\tt sl} that takes a list of natural numbers as input and returns
  the sum of all the elements in the list.
\item Prove that {\tt sl (lo n) = n * n}.
\item We define a function {\tt add} with the following code:
\begin{alltt}
Fixpoint add x y := match x with 0 => y | S p => add p (S y) end.
\end{alltt}
Prove the following lemmas:
\begin{enumerate}
\item {\tt forall x y, add x (S y) = S (add x y)}
\item {\tt forall x, add x 0 = x}
\item {\tt forall x y, add (S x) y = S (add x y)}
\item {\tt forall x y z, add x (add y z) = add (add x y) z}
\item {\tt forall x y, add x y = x + y}
\end{enumerate}
\item In the exercises part of the first chapter of these course
  notes, you are required to define a function that describes when a
  list of numbers is a licit representation of a natural number (it
  verifies that all digits are less than 10) and a function that computes the
  successor of a number when represented as a list of digits.  Add the
  function {\tt to\_nat} that maps any list of digits to the natural
  number it represents and show that the successor function is correct
  in this context.
\end{enumerate}

\section{More information}
You can use the book \cite{coqart} (available in French on internet, otherwise you should find English versions at the library) and
the reference manual \cite{coqmanual}.  There is also a tutorial in French \cite{surviecoq}.
There are also tutorials on the web \cite{coqtutorial,coqhurry}.
\begin{thebibliography}{10}
\bibitem{coqart}
Y.~Bertot and P.~Cast{\'e}ran.
\newblock {\em Interactive Theorem Proving and Program Development, Coq'Art:the
  Calculus of Inductive Constructions}.
\newblock Springer-Verlag, 2004.
\bibitem{softwarefoundations}
B.~Pierce et al.
\newblock{\em Software Foundations}
\newblock{\url{http://www.cis.upenn.edu/~bcpierce/sf/}}
\bibitem{coqhurry}
Y.~Bertot
\newblock{\em Coq in a Hurry}
\newblock Archive ouverte ``cours en ligne'',  2008.
\newblock {\url{http://cel.archives-ouvertes.fr/inria-00001173}}
\bibitem{coqmanual}
The Coq development team.
\newblock {\em The Coq proof Assistant Reference Manual},
\newblock Ecole Polytechnique, INRIA, Université de Paris-Sud, 2004.
\newblock {\url{http://coq.inria.fr/doc/main.html}}
\bibitem{coqtutorial}
G.~Huet, G.~Kahn, C.~Paulin-Mohring,
\newblock {\em The Coq proof Assistant, A Tutorial},
\newblock Ecole Polytechnique, INRIA, Université de Paris-Sud, 2004.
\newblock {\url{http://coq.inria.fr/V8.1/tutorial.html}}
\bibitem{tutreccoq}
E.~Giménez, P.~Castéran,
\newblock{\em A Tutorial on Recursive Types in Coq},
\newblock INRIA, Université de Bordeaux, 2006.
\newblock{\url{http://www.labri.fr/Perso/~casteran/RecTutorial.pdf.gz}}
\bibitem{surviecoq}
A.~Miquel,
\newblock{\em Petit guide de survie en Coq},
\newblock{Université de Paris VII}.
\newblock{\url{http://www.pps.jussieu.fr/~miquel/enseignement/mpri/guide.html}}
\end{thebibliography}

\end{document}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
