\documentclass{article}
\usepackage{url}
\usepackage{a4wide}
\usepackage{alltt}
\usepackage{color}
\usepackage[latin1]{inputenc}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqnot}{\mbox{\~{}}}
\newcommand{\coqor}{{\tt\char'134/}}
\title{Verifying programs and proofs\\
part VI. A glimpse at dependent types}
\author{Yves Bertot}
\date{November 2019}
\begin{document}
\maketitle
\section{Motivating introduction}
Proofs and programs are handled in the same way in
the langage of the Coq system (this language is also called the gallina
language).  This is a central aspect of Type Theory, the theory
that underlies the implementation of the system.  Studying this theory for its
expressive power is the subject of an entire teaching module, ({\em
  Type Theory}), but we wish to give a glimpse of it in this chapter.

\section{The {\em Curry-Howard} isomorphism}

The Coq system provides both a small purely functional programming
language and a logical system.  The key point of its design is that
the logical system and the programming language are only one language.

\subsection{Intuitive understanding of proofs-as-programs}
From a practical point of view, theorems are tools.  A theorem with the
statement \(A \Rightarrow B\) is a tool we can use to produce proofs of
\(B\) whenever we already have a proof of \(A\).  In this sense, it is
a function that takes as arguments proofs of \(A\) and produces proofs
of \(B\).  The idea of Type
Theory is to push this idea to the extreme so that any object of type
\(A \rightarrow B\) is actually a proof of the statement \(A
\Rightarrow B\).  So the type is the statement, or the other way
round, the statement is the type.  This is known as the {\em
  proposition-as-types} correspondence.

If we turn attention to proofs, the statement
\(A \Rightarrow B\) holds when we are able to produce an object \(p\)
whose type is \(A \rightarrow B\).  So when we write \(p : A \rightarrow
B\), we state that \(A \rightarrow B\) is a proposition and \(p\) is
a proof of this proposition.

The key insight is that this applies to other type constructions than
just the arrow type.  Every construct from the logical system
corresponds to a construct from the programming language and vice-versa.

When considering the programming language as a way to perform proofs, the
actual value of proofs is less important than the fact that they exist or not.
Part of the Coq system relies on a distinction between types that are
supposed to represent propositions and types that are supposed to represent
datatypes.  The type {\tt Prop} is used as type of types, with the
intent that the elements of {\tt Prop} are used to represent
propositions (statements).  For other types, there exist another type
of types, called {\tt Type}.   There is a form of subtype relation
here: a function that expects an element {\tt Type} as argument can be
applied to an element of {\tt Prop}, but a function that expects an
element of {\tt Prop} as argument cannot be applied to arbitrary
elements of {\tt Type}.

Beyond implication, the expressive power of the logical system relies
on universal quantification.  When we mirror the
behavior of universal quantification in the programming language, we
discover a feature that is usually not present in conventional programming
languages.  We have a statically typed language, but a single function
can return values in different types.

We can illustrate this with a simple predicate on natural numbers, for
example
the predicate {\tt even}, which expresses that a number is a double.
Using plain functional application, we have that {\tt even 0},
{\tt even 1} is a proposition, {\tt even 2} is a proposition, and so
on.  If we denote {\tt Prop} a type for propositions, then
{\tt even} must have type {\tt nat -> Prop}.  At this point, we should
note that {\tt even 0} and {\tt even 1} should be distinct
propositions, because the former should be provable (it should contain
a proof), and the second one should not.

When we perform a proof by induction for some property \(P(n)\) where
\(n\) is a natural number, the proof by induction actually leads to
verifying that \(P(0)\) holds
(we can call this information the base fact)
and that for every number \(n\),
\(P(n + 1)\) holds under the assumption that \(P(n)\) already holds
(we can call this information the step fact).  Intuitively, if we
combine the base fact and the step fact once, we get a proof of
\(P(1)\), then combining this proof of \(P(1)\) with the step fact
again, we can get a proof of \(P(2)\).  Obviously, for every natural
number \(k\), we only need to repeat this process \(k\) times to
obtain a proof of \(P(k)\).  However, any finite proof document would
only produce \(P(k)\) up to a certain bound.
The induction principle gives a way to describe 
this repetitive process and we shall see that the whole idea can be
described as a recursive function.  When we have the recursive
function, we have a stronger fact: the statement holds for any
natural number.  This is expressed by a statement \(\forall x:{\tt nat},
P(x)\) and the correspondence between propositions and types implies
that this statement should also be the type.

\section{dependent types}
In the same manner that a theorem that proves \(A \Rightarrow B\) is a
tool that constructs proofs of \(B\) from proofs of \(A\), a theorem
that proves \(\forall x : A, P(x)\) is a tool that constructs proofs of 
\(P(a_1)\), \(P(a_2)\), and so on for any any elements \(a_1\), \(a_2\) in
\(A\).  In this sense, theorems proving universally quantified statements
are also functions mapping objects to proofs, except that the input objects
may not be proofs, but regular data.

If the theorem \(th\) has the statement \(forall x:A,\, P(x)\) and
\(a_1\) is an element in \(A\), we can write \(th~a_1\) (the
application of \(th\) to \(a_1\)) and this gives us a proof of
\(P(a_1)\).  When applied to another argument \(a_2\), this gives us a
proof of \(P(a_2)\), this is really a different type.

The notation \(\forall (x : A), P(x)\) is often called a {\em product
  type}.  This is a reference to cartesian products.  When using a
cartesian product, or repeated cartesian products to obtain a tuple,
we have values in a family of types.  For instance, if the cartesian
product type \(A_1 * A_2\), then an element of this type provides a
guarantee to return an element of \(A_i\) for any \(i\) in \(\{1,2\}\).
A product type generalizes this situation to the case where the indices
are taken from any type instead of the finite set \(\{1,2\}\).  If we
have a function of type \(\forall x: A, P(x)\) we also guarantee the
existence of elements in \(P(x)\) for any possible index \(x\).

\subsection{Functions with dependent types}
In practice, this means that we have two markers for function types.
The one that we already know is the ``arrow'' type, which have been
using for all functions in previous lessons and which we saw in the
previous section that it can also be read as type for implication.
The new function type is the universal quantification, sometimes also
called a product type.  Understanding how these are used in well
formed formulas revolves around the following two sentences.
\begin{itemize}
\item if a function \(f\) has type \(A \rightarrow B\) and a value
  \(e\) has type \(A\), the the expression \(f~e\) is well formed and
  has type \(B\).
\item if a function \(f\) has type \(\forall x: A,\, B\) and a value
  \(e\) has type \(A\) then the expression \(f~e\) is well formed and
  has type \(B\) where all free occurences of \(x\) are replaced by \(e\).
\end{itemize}

In reality, the Coq system does not have two rules for typing function
calls, it only knows the rule for functions with a type of the form
\(forall x: A,\, B\).  The trick that Coq uses is that universally
quantified formulas such that \(x\) does not occur in \(B\) are
automatically printed as arrow types.

\begin{alltt}
Check (forall x: nat, nat).
\textcolor{blue}{nat -> nat : Set}
\end{alltt}

In the Coq system, proofs are always represented as programs in the
same programming language that we used for our simple programs.  The
goal-directed machinery and the tactics that we learned to use in the
previous lessons are only provided to help us construct these proofs,
but we can play the game of constructing proofs without using tactics.
Actually an expert user of Coq will regularly have small fragments of
proofs built directly by applying theorems to values in the middle of
their tactic scripts.

A very simple example of proposition that is always true and can be
described using implication and universal quantification is the
proposition {\em for every proposition \(A\), if \(A\) holds then \(A\)
  holds}.  A proof of this proposition can be written directly in
the Coq system without using tactics in the following manner.
\begin{alltt}
Definition basic_truth : forall A : Prop, A -> A :=
  fun (E : Prop)(x : E) => x.
\end{alltt}
This is an example of a tautology.  As often, this tautology is not very
interesting, it does not teach us much.  We shall only use it when
we need a provable proposition somewhere.

A very simple example of a proposition that cannot be proved is the
following one:
\begin{alltt}
Check forall A : Prop, A.
\textcolor{blue}{forall A : Prop, A 
   : Prop}
\end{alltt}
That this formula cannot be proved is the central fact guaranteeing
that the Coq system is consistent.
\subsection{Constructing proofs directly}
If we have a theorem \(th\) whose statement is \(\forall x : A, P~x \rightarrow
Q~x\), this means that \(P\) has type \(A \rightarrow {\tt Prop}\),
\(Q\) has type \(A \rightarrow {\tt Prop}\), and \(th\) is actually a
function accepting two arguments.  To illustrate this, we shall use an
example with two theorems that are always loaded at the beginning of a
Coq session.

These statements are concerned with the two-place predicate \(. \leq .\)
\begin{alltt}
Check le.
\textcolor{blue}{le : nat -> nat -> Prop}
\end{alltt}
This predicate is about two natural numbers.  When these numbers are
given, a specific notation is triggered.

\begin{alltt}
Check le 3 1.
\textcolor{blue}{3 <= 1}
\end{alltt}

Among the basic theorems that are provided for {\tt le}, here are two
that we will use in this illustration.
\begin{alltt}
Check le_n.
\textcolor{blue}{le_n : forall n : nat, n <= n}

Check le_S.
\textcolor{blue}{le_n : forall n m : nat, n <= m -> n <= S m}
\end{alltt}

Theorem \verb+le_n+ is a function that takes only one argument.
Theorem \verb+le_S+ is a function that takes three arguments, two
natural numbers and a proof that these numbers respect {\tt le}.

For instance, we wish to construct a proof of {\tt 3 <= 5}.  We will
proceed in the following manner.

\begin{itemize}
\item First we note that {\tt le\_n 3} is a proof of {\tt 3 <=
  3}.
\item
Then {\tt le\_S 3} is a proof of {\tt forall m, 3 <= m ->
  3 <= S m}
\item
{\tt le\_S 3 3} is a proof of {\tt 3 <= 3 -> 3 <= 4}
\item
This is a function, it expects an element of type {\tt 3 <= 3} as
argument, but we know how to produce one: {\tt le\_n 3}, so we can
continue
\item
{\tt le\_S 3 3 (le\_n 3)} is a proof of {\tt 3 <= 4}
\item
{\tt le\_S 3 4} is a proof of {\tt 3 <= 4 -> 3 <= 5}
\item
This is a function, it expects an element of type {\tt 3 <= 4} as
argument so we can
continue
\item
{\tt le\_S 3 4 (le\_S 3 3 (le\_n 3))} is a proof of {\tt 3 <= 5}.
\item
We can use \verb+Check le_S 3 4 (le_S 3 3 (le_n 3)).+ to verify this with the
Coq system.
\end{itemize}

\label{larger0}
If we wanted to prove that 10 is larger than 0, it would be feasible
by this approach.
If we wanted to prove that 100 is larger than 0, it would be feasible but
tedious.  We need to find a way to avoid this kind of proof expansion.
\section{dependently typed pattern-matching}
Functions with dependent types are rarely provided by conventional
programming languages.

In the programming language of the Coq system, we can construct a
function that returns a dependent type in a principled way.  Even for a
function acting on natural numbers and in charge of returning values in
different types.  The main tool is {\em dependent pattern-matching},
where the user can instruct the Coq system to construct explicitely
objects of different types in each of the branches.

A general form of recursive function on the type of natural numbers
would have the following shape.
\begin{alltt}
Fixpoint gen_rec ... (n : nat) : ... :=
  match n with
  | O => ...
  | S p => ...
  end.
\end{alltt}
First we would like to state that the returned value will be in a type
written as {\tt B n} for some {\tt B}.  The first trick we will use is to
make this {\tt B} an argument of the function.  What we want is that 
{\tt B n} should be a type whenever {\tt n} is a natural number.  To
say this, we state that {\tt gen\_rec} has a first argument \hbox{\tt B :
  nat -> Type}.  Our definition takes the following form:
\begin{alltt}
Fixpoint gen_rec (B : nat -> Type) ... (n : nat) : B n :=
  match n with
  | O => ...
  | S p => ...
  end.
\end{alltt}
Now we need to instruct the Coq system so that it understands that the
branches of the pattern matching construct should return a different
type, this is done by the following syntax:
\begin{alltt}
Fixpoint gen_rec (B : nat -> Type) ... (n : nat) : B n :=
  match n return B n with
  | O => ...
  | S p => ...
  end.
\end{alltt}
This says that the first branch should return a value in type {\tt B
  O}, and the second branch should return a value in {\tt B (S p)}.

For the branch on \(0\), we know that the returned value should have
type {\tt B 0}.  Since {\tt B} is completely unknown, we don't know
how to construct such a value, so we will simply assume it is given as
an argument to the function {\tt gen\_rec}.
\begin{alltt}
Fixpoint gen_rec (B : nat -> Type) (V : B 0)... (n : nat) : B n :=
  match n return B n with
  | O => V
  | S p => ...
  end.
\end{alltt}
For the second branch, we know the returned value should have type
{\tt B (S p)}, but we also know that this value can be built using
{\tt p} and the result of a recusive call on {\tt p}.
This recusive call will produce a value of
type {\tt B p}.  So all the computation happening in the second branch
can be modeled by some function {\tt F} that takes {\tt p} as
argument, a value in type {\tt B p} and has to produce a result in
type {\tt S p}.  This function {\tt F} and must have
type {\tt forall p : nat, B p -> B (S p)}.
As for {\tt V} we don't know enough
to invent this {\tt F} so we assume it is given as argument to the
{\tt gen\_rec}
function.  In all the complete code for the function looks as follows:

\begin{alltt}
Fixpoint gen_rec (B : nat -> Type) (V : B 0)
  (F : forall p : nat, B p -> B (S p)) (n : nat) : B n :=
  match n return B n with
  | O => V
  | S p => F p (gen_rec B V F p)
  end.
\end{alltt}

We shall now illustrate how such a function could be used, for instance to
construct proofs.  For instance, we may want to prove that every
natural number is larger than 0, as was mentioned in section~\ref{larger0}.
We will use the function {\tt gen\_rec} with the type family {\tt B}
fixed to {\tt fun n : nat => 0 <= n}.  For value {\tt V}, we need a
value of type {\tt 0 <= 0}.  We can construct such a value using {\tt
  le\_n}.
For {\tt F}, we need to have the type {\tt forall p, 0 <= p -> 0 <= S
  p}.  It turns out that {\tt le\_S 0} has exactly this type.  So we
can instantiate {\tt gen\_rec} with these 3 arguments.

\begin{alltt}
Definition proof_0_le : forall x, 0 <= x :=
  gen_rec (fun x => 0 <= x) (le_n 0) (le_S 0).
\end{alltt}

Two more remarks need to be done about this proof.  First, users
the {\tt gen\_rec} function already exists
inside the Coq system, but under a different name.
It was generated automatically when the type
{\tt nat} was defined inductively.  The name of the function is {\tt
  nat\_rect} (the suffix {\tt rect} should be broken in two parts:
{\tt rec} because it is the generic recursive function on natural
numbers, and {\tt t} because it is defined for any type).  In general,
this generic recursive function are named {\em induction principles}.
Second,
this function is interesting because it exists, but it rarely useful to
execute it, otherwise it will construct the long sequence of nested
theorems that were mentioned in Section~\ref{larger0}.

\begin{alltt}
Check proof_0_le 10.
\textcolor{blue}{proof_0_le 10 : 0 <= 10}

Compute proof_0_le 10.
\textcolor{blue}{le_S 0 9 (le_S 0 8 (le_S 0 7 (le_S 0 6 (le_S 0 5
    (le_S 0 4 (le_S 0 3 (le_S 0 2 (le_S 0 1 (le_S 0 0 (le_n 0))))))))))}
\end{alltt}
When it comes to proofs, the command {\tt Check} is more important
than {\tt Compute}.
\section{Dependent inductive types}
The inductive types that we saw in previous sections have all their
constructors with simple types.  It is interesting to see what happens
if use inductive definitions for {\em families} of types, with
constructors who have dependent types.

As an illustration, we shall consider a few types whose elements are
indexed by a natural number. These types will thus have type {\tt nat
  -> Type}.

The first example is a family of types for all natural numbers, but
only a few of these types have elements.
\begin{alltt}
Inductive lt3 : nat -> Type :=
| lt3_0 : lt3 0
| lt3_1 : lt3 1
| lt3_2 : lt3 2.
\end{alltt}
With such a definition, the type {\tt lt3 3} exists, but it does not
contain any lement.  On the other the type {\tt lt3 0} exists, but it
contains only element.  This an example of a type that could be used
to represent proofs, but for now will keep it as a datatype.

If we want to define a function on {\tt lt3} we need to cover all
possible indices in {\tt nat}, so this function will have a type of
the form {\tt forall (n : nat)(t : lt3 n), ...}.  For instance, the
generic function by dependent pattern matching would have the
following form:
\begin{alltt}
Definition lt3_gen (B : forall n : nat, lt3 n -> Type) ...
  (x : nat) (t : lt3 x) :=
  match x with
  | lt3_0 => ...
  | lt3_1 => ...
  | lt3_2 => ...
  end.
\end{alltt}
We need to express that each of the branches of the pattern-matching
construct will return a different type.  This is done by modifying
the pattern-matching construct in the following manner:
\begin{alltt}
Definition lt3_gen (B : forall n : nat, lt3 n -> Type) ...
  (x : nat) (t : lt3 x) :=
  match x in lt3 a return B a x with
  | lt3_0 => ...
  | lt3_1 => ...
  | lt3_2 => ...
  end.
\end{alltt}
Because the constructor in the first matching close has type {\tt lt3
  0}, this means that the first branch will have to return a value
of type {\tt B 0 lt3\_0}.  We don't know how to construct such a
value, because we don't know anything about {\tt B}, so we shall assume

\begin{alltt}
Inductive ge3 : nat -> Type :=
  ge3n : ge3 3
| ge3S : forall m, ge3 m -> ge3 (S m).
\end{alltt}
Obviously, the type {\tt ge3 3} contains an element, {\tt ge3n}.  If
we want to construct an element of {\tt ge3 4}, we can do so by
combining {\tt ge3n} with {\tt ge3S}.
\begin{alltt}
Check ge3S 3 ge3n.
\textcolor{blue}{ge3S 3 ge3n : ge3 4}
\end{alltt}
We can construct an element of {\tt ge3 10} by repeating this process
again and again.  A nice trick is to avoid having to fill up the
numeric values, the Coq system can do that for us if we leave blank
placeholders.
\begin{alltt}
Check ge3S _ (ge3S _ (ge3S _ (ge3S _ (ge3S _ (ge3S _ (ge3S _ (ge3n))))))).
\textcolor{blue}{ge3S 9 (ge3S 8 (ge3S 7 (ge3S 6 (ge3S 5 (ge3S 4 (ge3S 3 ge3n)))))) : ge3 10}
\end{alltt}
We can also use our {\tt gen\_rec} function to build arbitrary proofs,
but this is quite tricky\footnote{this example can be kept for later.}
\begin{alltt}
Check (gen_rec (fun x=> ge3 (x + 3)) ge3n (fun m => ge3S (m + 3)) 7) : ge3 10.
\textcolor{blue}{gen_rec (fun x : nat => ge3 (x + 3)) ge3n (fun m : nat => ge3S (m + 3)) 7
:
ge3 10
     : ge3 10}
\end{alltt}

We can also construct a generic recursive function for the type {\tt
  ge3}, this construction follows the same pattern as for {\tt
  gen\_rec}.

We want to construct a function that takes all elements of all the
types in the inductive family {\tt ge3}, so the statement we want to
obtain has the form:
\begin{alltt}
forall (n : nat) (t : ge3 n), C n t
\end{alltt}
We use {\tt C} for {\tt ge3} as a parallel to the
variable {\tt B} that we used on {\tt nat} in {\tt gen\_rec}.

For this statement to be well formed, we need {\tt C} to have type
\begin{alltt}
C : forall n : nat, ge3 n -> Type
\end{alltt}

In the definition of {\tt gen\_rec}, we used a value {\tt V} that had
type {\tt B 0}.  This time we need, a value that fits the first
constructor {\tt ge3n} of the type {\tt ge3}.   This value will need
to have type {\tt C 3 ge3}.  This time we choose to name this value
{\tt W}.

For the second constructor, it has two arguments, the first one is
a natural number {\tt n} and the second argument {\tt t}
is an element of the type
{\tt ge3 n}, which is in the same inductive family.  For this second
argument, a recursive call is also possible, and it return a value in
the type {\tt C n t}.  Using these three values, the whole branch
must construct a value of type {\tt C (S n) (ge3S n t)}.  This time,
we choose to name this value {\tt G}.

Putting all together, we get the following definition that really has
the same shape as the definition for {\tt gen\_rec}.
\begin{alltt}
Fixpoint ge3_gen_rec (C : forall n : nat, ge3 n -> Type)
  (W : C 3 ge3n)
  (G : forall (n : nat) (t : ge3 n), C n t -> C (S n) (ge3S n t))
  (m : nat) (g : ge3 m) :=
match g in ge3 k return C k g with
| ge3n => W
| ge3S p t => G p t (ge3_gen_rec C W G p t)
end.
\end{alltt}
The type of {\tt ge3\_gen\_rec} can be displayed with the help of the
Coq system.
\begin{alltt}
Check ge3_gen_rec.
\textcolor{blue}{ge3_gen_rec
     : forall C : forall n : nat, ge3 n -> Type,
       C 3 ge3n ->
       (forall (n : nat) (t : ge3 n), C n t -> C (S n) (ge3S n t)) ->
       forall (m : nat) (g : ge3 m), C m g}
\end{alltt}
If we want to use such an inductive type only for proofs, the value of
each element of {\tt ge3 \(n\)} is irrelevant, only their existence
matters.  In this case, it is worth instantiating this general
induction principle with a proposition that does look at the values.
It makes it possible to obtain a simpler induction principle.

\begin{alltt}
Definition ge3_gen_ind :
  forall (P : nat -> Prop), P 3 -> (forall k, ge3 k -> P k -> P (S k)) ->
    forall (n : nat), ge3 n -> P n :=
  fun P (V : P 3) (H : forall k, ge3 k -> P k -> P (S k)) =>
  ge3_gen_rec (fun (x : nat) (t : ge3 x) => P x) V H.

Check ge3_gen_ind.
\textcolor{blue}{ge3_gen_ind
     : forall P : nat -> Prop,
       P 3 ->
       (forall k : nat, ge3 k -> P k -> P (S k)) ->
       forall n : nat, ge3 n -> P n}
\end{alltt}

Another example of inductive type that can be used to understand
general induction principles is the inductive definition of a property
that is satisfied only by numbers that are equal to 3.  This inductive
type is actually close to {\tt ge3} except that there is only one
constructor.
\begin{alltt}
Inductive is3 : nat -> Prop :=
  is3_refl : is3 3.
\end{alltt}
The constructor of this inductive predicate has no subterm, so no
recursion is needed to define its induction principle.  It goes as
follows:
\begin{alltt}
Definition is3_gen (P : forall (x : nat), is3 x -> Type)
   (V : P 3 is3_refl) (y : nat) (t : is3 y) : P y t :=
  match t in is3 k return P k t with
  | is3_refl => V
  end.

Definition is3_gen_ind :
  forall (P : nat -> Prop), P 3 -> forall y, is3 y -> P y :=
  fun P (V : P 3) =>
  is3_gen (fun x t => P x) V.

Check is3_gen_ind.
\textcolor{blue}{is3_gen_ind
     : forall P : nat -> Prop, P 3 -> forall y : nat, is3 y -> P y}
\end{alltt}
We can use this inductive type as a precursor to understand what
happens with equality, which we shall see again in the next section.
It should be noted that {\tt is3\_gen\_ind} expresses that when {\tt
  y} satisfies the predicate {\tt is3}, to prove that {\tt y}
satisfies any property, we simply need to show that this property is
satisfied by {\tt 3}.  Equality behaves the same, except that it works
for any type and for any element of that type.

\subsection{Inductive predicates}
In Coq many predefined logical connective are actually given as
inductive types.  The most characteristic is equality, which looks
like {\tt ge3}, except that there is only one constructor and equality
is actually described as a proposition (not as a general datatype).
\begin{alltt}
Inductive eq (A : Type) (x : A) : A -> Prop :=
  eq_refl : eq A x x.
\end{alltt}
The inductive principle, {\tt eq\_ind} associated to {\tt eq} has the
same shape as the inductive principle {\tt is3\_gen\_ind} from the
previous section.  It expresses that when {\tt x = y} holds, if we
want to prove that {\tt y} satisfies some property {\tt P}, we only
have to show that {\tt x} does satisfy {\tt P}.
\begin{alltt}
Check eq_ind.
\textcolor{blue}{eq_ind
     : forall (A : Type) (x : A) (P : A -> Prop),
       P x -> forall y : A, x = y -> P y}
\end{alltt}

In Coq logical connectives like {\tt \coqand{}} and {\tt \coqor{}} are
actually given as inductive definitions.  Existential quantification, too.

\section{References}
The book Interactive Theorem Proving and Program Development: Coq'Art:
The Calculus of Inductive Constructions, by Bertot and Cast�ran gives
more explanations about the variety of inductive propositions (in
chapter 6) and about the construction of induction principles (in
chapter 14).  The French version is available online \url{https://www.labri.fr/perso/casteran/CoqArt/index.html}.


\end{document}
