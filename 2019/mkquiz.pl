#!/usr/bin/env perl

# to use this file, perl -f  <this file>  < input.txt > output.md
# beware that the solutions are always given in file
# solutions.md
# output markdown file can then be processed with the following
# command:
# python3 -m markdown output.md > output.html
# python3 -m markdown solutions.md > solutions.html
# rename files at your convenience.

$debug = 0;

open(SOL, ">solutions.md");
open(DEBUG, ">debug.trace") if $debug;

$question_no = 1;
while (<>) {
  print DEBUG "looking at $.:$_";
  if(/^\*\*\* (.*)/) {
    print "\# $1\n";
  }
  if(/^=== main text/){
    $main="";
    GETMAIN : while(<>) {
      if(/^===/) {
        last GETMAIN;
      } else {
        $main=$main . $_;
      }
    };
    $correctanswer = "";
    GETCORRECT: while(<>) {
      if (/^===/) {
        last GETCORRECT;
      } else {
        $correctanswer = $correctanswer . $_ ;
      }
    };
    $firstdistractor = "";
    GETDISTRACT1 : while(<>) {
      if (/^===/) {
        last GETDISTRACT1;
      } else {
        $firstdistractor = $firstdistractor . $_ ;
      }
    };
    $seconddistractor = "";
     GETDISTRACT2: while(<>) {
      if (/^===/) {
        last GETDISTRACT2;
      } else {
        $seconddistractor = $seconddistractor . $_ ;
      }
    };
    $thirddistractor = "";
    GETDISTRACT3 : while(<>) {
      if (/^===/ || /^\*\*\*/) {
        last GETDISTRACT3 ;
      } else {
        $thirddistractor = $thirddistractor . $_ ;
      }
    };
    @text=($correctanswer, $firstdistractor, $seconddistractor, $thirddistractor);
    print DEBUG "question $question_no at line $. <---\n";
    @indices = (0,1,2,3);
    $k = int (rand(4));
    print DEBUG "draw < 4 : $k\n";
    @perm = ($indices[$k]);
    while ($k < 3) {
      $indices[$k] = $indices[$k + 1];
      $k = $k + 1;
    }
    print DEBUG "remaining indices $indices[0] $indices[1] $indices[2]\n";
    $k = int (rand(3));
    print DEBUG "draw < 3 : $k\n";
    @perm = (@perm, $indices[$k]);
    while ($k < 2) {
	$indices[$k] = $indices[$k + 1];
	$k = $k + 1;
    }
    print DEBUG "remaining indices $indices[0] $indices[1]\n";
    $k = int (rand(2));
    print DEBUG "draw < 2 : $k\n";
    @perm = (@perm, $indices[$k], $indices[1 - $k]);

    print STDOUT "\n### Question $question_no:\n";
    print STDOUT $main;
    print SOL "\n###Question $question_no:\n";
    $question_no = $question_no + 1;
    print SOL $main;
    $correctanswergiven=0;
    $j = 0;
    while($j < 4) {
	$l = $j + 1;
	print STDOUT "\n\nanswer $l:\n";
	print STDOUT $text[$perm[$j]];
	if ($perm[$j] == 0) {
	    print SOL "the correct answer is answer no. $l:\n";
	    print SOL $text[0]
	}
	$j = $l;
    }
  }
}
